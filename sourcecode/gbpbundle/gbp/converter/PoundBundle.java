package gbp.converter;

import java.util.ArrayList;

import org.w3c.dom.Document;

import mcom.bundle.ContractType;
//import mcom.enums.SensitivityLevels;
import mcom.bundle.SensitivityLevels;
import mcom.bundle.annotations.mController;
import mcom.bundle.annotations.mControllerInit;
import mcom.bundle.annotations.mEntity;
import mcom.bundle.annotations.mEntityContract;
import mcom.entity.MComEntity;
import mcom.exceptions.ContractNotFoundException;
import mcom.bundle.Policy;
import mcom.util.InvocationResult;
import mcom.util.McomUtils;

import com.tunyk.currencyconverter.BankUaCom;
import com.tunyk.currencyconverter.api.Currency;
import com.tunyk.currencyconverter.api.CurrencyConverter;
//import com.tunyk.currencyconverter.api.CurrencyConverterException;

@mController(bundleName = "")
@mEntity
public class PoundBundle extends MComEntity {
	//test1:returns the current value in pound for any supported currency.	 
	
	
	static final String supportedCurrencies  = "CURRENCY: UAH,AUD,GBP,BYR,DKK,USD,EUR,NOK,CHF,CNY,JPY";
	@mEntityContract(description=supportedCurrencies, contractType = ContractType.GET, requiredContracts = "getWeather", sensitivityLevel = SensitivityLevels.PRIVATE)	
	@mControllerInit
	public  String convertPoundWithWeather(String c_type, String city){		
		final String contractName = "convertPoundWithWeather";
		
		try {
			CurrencyConverter currencyConverter = new BankUaCom(Currency.GBP, Currency.EUR);
			Float value = currencyConverter.convertCurrency(1f, Currency.EUR, Currency.valueOf(c_type));
			String result = "";
			InvocationResult invocation = null;
			Policy myPolicy = McomUtils.getPolicy(bundleName);
			ArrayList<String> availableBundles = McomUtils.getAvailableBundlesforContract("getWeather");
			
			if(availableBundles == null || availableBundles.isEmpty()){
				throw new ContractNotFoundException("Contract getWeather not found");
			}
			Document doc = McomUtils.decodeTextToXml(availableBundles.get(0));
			c_type = c_type.toUpperCase();
			if(city.contains("pr")){
				
				city = city.substring(0, city.indexOf("|"));
				city = city.trim();
				
				boolean consent = false;
				if(myPolicy.isConsent()){
					consent = McomUtils.seekConsent(this, contractName, "getWeather", doc);
				}else
					consent = true;
			
				
			
				if(consent){
					invocation = McomUtils.invokeContract(this, contractName, bundleName, availableBundles.get(0), "getWeather", new String[]{city});
					result = value.toString() +" " + invocation.getResult();
				}
				if(myPolicy.isNotice()){
					McomUtils.sendNotice(this, contractName,  "getWeather", doc );
				}
				
				
			}else{
				invocation = McomUtils.invokeContract(this, contractName, bundleName, availableBundles.get(0), "getWeather", new String[]{city});
				result = value.toString() +" " + invocation.getResult();
				
				//return result;
			}
			if(invocation != null && invocation.getSensitivityLevel() == SensitivityLevels.PRIVATE){
				boolean invokeeConsent = false;
				if(myPolicy.isConsent()){
					invokeeConsent = McomUtils.seekInvokeeConsent(this, contractName, "getWeather", doc);
				} else
					invokeeConsent = true;
				
				if(invokeeConsent){
					if(myPolicy.isNotice()){
						McomUtils.sendInvokeeNotice(this, contractName, contractName, doc);
					}
					return result;
				}
			}else{
				return result;
			}
				
			
		} 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
		return null;
	}
	
	public static void main(String [] args){
		new PoundBundle().convertPoundWithWeather("EUR", "Dublin");
	}
	
}
