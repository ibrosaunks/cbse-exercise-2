package gbpbad.converter;

import java.util.ArrayList;

import org.w3c.dom.Document;

import mcom.bundle.ContractType;
import mcom.bundle.SensitivityLevels;
import mcom.bundle.annotations.mController;
import mcom.bundle.annotations.mControllerInit;
import mcom.bundle.annotations.mEntity;
import mcom.bundle.annotations.mEntityContract;
import mcom.entity.MComEntity;
import mcom.bundle.Policy;
import mcom.util.McomUtils;

import com.tunyk.currencyconverter.BankUaCom;
import com.tunyk.currencyconverter.api.Currency;
import com.tunyk.currencyconverter.api.CurrencyConverter;
//import com.tunyk.currencyconverter.api.CurrencyConverterException;

@mController(bundleName = "")
@mEntity
public class PoundBundle extends MComEntity {
	//test1:returns the current value in pound for any supported currency.	 
	
	
	static final String supportedCurrencies  = "CURRENCY: UAH,AUD,GBP,BYR,DKK,USD,EUR,NOK,CHF,CNY,JPY";
	@mEntityContract(description=supportedCurrencies, contractType = ContractType.GET, sensitivityLevel =  SensitivityLevels.PUBLIC)	
	@mControllerInit
	public  Float convertPoundLite(String c_type, String city){		
		final String contractName = "convertPoundLite";
		try {
			CurrencyConverter currencyConverter = new BankUaCom(Currency.GBP, Currency.EUR);
			Float value = currencyConverter.convertCurrency(1f, Currency.EUR, Currency.valueOf(c_type));
			Policy myPolicy = McomUtils.getPolicy(bundleName);
			ArrayList<String> availableBundles = McomUtils.getAvailableBundlesforContract("getWeather");
			
			Document doc = McomUtils.decodeTextToXml(availableBundles.get(0));
			//boolean consent;
			
			
			McomUtils.invokeContract(this, contractName, bundleName, availableBundles.get(0), "getWeather", new String[]{city});
				
		
			return value;
		} 
		catch (Exception e) {
			e.printStackTrace();

		}		
		return null;
	}
	
	public static void main(String [] args){
		
		new PoundBundle().convertPoundLite("EUR", "Glasgow");
	}
	
}
