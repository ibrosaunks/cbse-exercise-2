package mcom.entity;



import mcom.bundle.Policy;

public abstract class MComEntity {
	
	public Integer bundleId;
	
	public String bundleName;
	
	public String ip_port;
	
	public String invoker_ip_port;
	
	public Long invocationId;
	
	public String origin_ip_port;
	
	public Policy originPolicy;
	
}
