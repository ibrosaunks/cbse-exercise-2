package mcom.util;


import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import mcom.bundle.SensitivityLevels;
import mcom.entity.MComEntity;
import mcom.bundle.Policy;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class McomUtils {
	
	public static ArrayList<String> getAvailableBundlesforContract(String contractName){
		try{
		Class remoteLookupClass = ClassLoader.getSystemClassLoader().loadClass("mcom.wire.util.RemoteLookupService");
		//System.out.println("remoteLookupClass found: " + remoteLookupClass.getCanonicalName());
		Method findBundle = remoteLookupClass.getMethod("findBundlesByContract", new Class[]{String.class});
		ArrayList<String> contractBundles = (ArrayList<String>) findBundle.invoke(null, new Object[]{contractName});
		
		return contractBundles;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public static Policy getPolicy(String bundleName){
		try{
			Class kernelUtilClass = ClassLoader.getSystemClassLoader().loadClass("mcom.kernel.util.KernelUtil");
			Method getPolicyMethod = kernelUtilClass.getMethod("loadPolicy", new Class[]{String.class});
			
			Document doc = (Document) getPolicyMethod.invoke(null, new Object[]{bundleName});
			Policy bundlePolicy = new Policy();
			bundlePolicy.setNotice(Boolean.parseBoolean(doc.getElementsByTagName("Notice").item(0).getTextContent()));
			bundlePolicy.setConsent(Boolean.parseBoolean(doc.getElementsByTagName("Consent").item(0).getTextContent()));
			return  bundlePolicy;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public static Document decodeTextToXml(String dtText){
		Document doc = null;
		try(ByteArrayInputStream stream = new ByteArrayInputStream(dtText.getBytes("utf-8"))) {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			
			//doc = dBuilder.parse(dtText);
			doc = dBuilder.parse(new InputSource(stream));
			doc.getDocumentElement().normalize();
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return doc;
	}
	
	public static boolean isSandboxed(Document doc){
		
		boolean sandboxed = Boolean.parseBoolean(doc.getElementsByTagName("IsSandboxed").item(0).getTextContent());
		
		
		return sandboxed;
	}
	
	public static String getStringAttribute(Document doc, String attribute){
		
		
		return doc.getElementsByTagName(attribute).item(0).getTextContent();
	}
	public static InvocationResult invokeContract(MComEntity entity, String contractName, String bundleName, String bundleDesc, String bundleContractName, String[] arguments){
		
		try {
			
			//System.out.println("Contracts gotten: " + contractBundles);
			
			String[] invocationDetails = new String[]{entity.ip_port, contractName, String.valueOf(entity.bundleId),  entity.bundleName, entity.invoker_ip_port};
				
			Class remoteInvocationClass = ClassLoader.getSystemClassLoader().loadClass("mcom.kernel.impl.StubImpl");
				
			Method interProcessInvocation = remoteInvocationClass.getMethod("interContractInvocation", new Class[]{Long.class, String[].class, String.class, String.class, String[].class});
				
			Object result = interProcessInvocation.invoke(null, new Object[]{entity.invocationId, invocationDetails, bundleDesc, bundleContractName, arguments} );
			
			String[] resultArr = (String[]) result;
			/*if(entity.privateParams != null){
			for(int i = 0; i < arguments.length;i++){
			
			}
			}*/
			return new InvocationResult(resultArr[0], getSensitivityLevelFromString(resultArr[1]));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean seekConsent(MComEntity bundle, String contractName, 
			String contractToInvoke, Document contractToInvokeBundleDescriptor) {
		String[] header = null;
		String bundleToInvoke = getStringAttribute(contractToInvokeBundleDescriptor, "BundleName");
		String bundleToInvokeId = getStringAttribute(contractToInvokeBundleDescriptor, "BundleId");
		String[] notifieeDetails;
		if(bundle.origin_ip_port == null){
			header = bundle.invoker_ip_port.split("__");
			notifieeDetails = new String[]{bundle.invoker_ip_port, contractToInvoke, bundleToInvokeId, bundleToInvoke};
		}
		else{
			header = bundle.origin_ip_port.split("__");
			notifieeDetails = new String[]{bundle.origin_ip_port, contractToInvoke, bundleToInvokeId, bundleToInvoke};
		}
		try(Socket consentSocket = new Socket(header[0], Integer.parseInt(header[1]));
			DataInputStream in = new DataInputStream(consentSocket.getInputStream());
			DataOutputStream out = new DataOutputStream(consentSocket.getOutputStream());
		){
			
			Class sandboxLoggerClass = ClassLoader.getSystemClassLoader().loadClass("mcom.sandbox.SandboxLogger");
			
			// Long invocationID, String[] notifier, String[] notifiee, String notifierType, Boolean granted
			
			Method writeNotificationLog = sandboxLoggerClass.getMethod("writeNotificationLog", new Class[]{ Long.class, String[].class,  String[].class, String.class, Boolean.class});
			
			StringBuilder message = new StringBuilder();
			message.append("CONSENTHEADER-" + bundle.ip_port);
			message.append(System.getProperty("line.separator"));
			message.append("CONSENTBODY-");
			message.append(contractName + " contract in " + bundle.bundleName + " bundle is hereby seeking permission to share your information with " + contractToInvoke);
			message.append("\n contract in " + bundleToInvoke + " bundle ");
			
			
			message.append("\n Do you wish to grant this permission? (yes/no)");
			
			
			
			out.writeUTF(message.toString()); // UTF is a string encoding;
			
			String response = in.readUTF();
			
			boolean granted = false;
			
			if(response.equalsIgnoreCase("yes") || response.equalsIgnoreCase("y"))
				granted =  true;
			else if(response.equalsIgnoreCase("no") || response.equalsIgnoreCase("no"))
				granted =  false;
			String[] notifierDetails = new String[]{bundle.ip_port, contractName, String.valueOf(bundle.bundleId), bundle.bundleName};
			
			
			
			out.writeUTF(bundle.ip_port +" ack");
			writeNotificationLog.invoke(null, new Object[]{bundle.invocationId,notifierDetails, notifieeDetails, "consent", granted });
			return granted;
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}
	
	public static boolean seekInvokeeConsent(MComEntity bundle, String contractName, 
			String contractToInvoke, Document contractToInvokeBundleDescriptor){
		
		
		String bundleToInvoke = getStringAttribute(contractToInvokeBundleDescriptor, "BundleName");
		String bundleToInvokeId = getStringAttribute(contractToInvokeBundleDescriptor, "BundleId");
		String bundleHostAddress = getStringAttribute(contractToInvokeBundleDescriptor, "HostAddress");
		String bundleHostPort = getStringAttribute(contractToInvokeBundleDescriptor, "HostPort");
		String bundleHostNotificationPort = getStringAttribute(contractToInvokeBundleDescriptor, "HostNotificationPort");
			
		try(Socket consentSocket = new Socket(bundleHostAddress, Integer.parseInt(bundleHostNotificationPort));
			DataInputStream in = new DataInputStream(consentSocket.getInputStream());
			DataOutputStream out = new DataOutputStream(consentSocket.getOutputStream());
		){
			
			Class sandboxLoggerClass = ClassLoader.getSystemClassLoader().loadClass("mcom.sandbox.SandboxLogger");
			
			// Long invocationID, String[] notifier, String[] notifiee, String notifierType, Boolean granted
			
			Method writeNotificationLog = sandboxLoggerClass.getMethod("writeNotificationLog", new Class[]{ Long.class, String[].class,  String[].class, String.class, Boolean.class});
			
			StringBuilder message = new StringBuilder();
			message.append("CONSENTINVOKEEHEADER-" + bundle.ip_port);
			message.append(System.getProperty("line.separator"));
			message.append("CONSENTINVOKEEBODY-");
			message.append(bundle.bundleName + " bundle located in " + bundle.ip_port + " and being invoked by " + bundle.invoker_ip_port + " is hereby seeking permission to share your result with an invoker ");
			
			
			message.append("\n Do you wish to grant this permission? (yes/no)");
			
			
			
			out.writeUTF(message.toString()); // UTF is a string encoding;
			
			String response = in.readUTF();
			
			boolean granted = false;
			
			if(response.equalsIgnoreCase("yes") || response.equalsIgnoreCase("y"))
				granted =  true;
			else if(response.equalsIgnoreCase("no") || response.equalsIgnoreCase("no"))
				granted =  false;
			String[] notifierDetails = new String[]{bundle.ip_port, contractName, String.valueOf(bundle.bundleId), bundle.bundleName};
			String[] notifieeDetails = new String[]{bundleHostAddress + "__" + bundleHostPort, contractToInvoke, bundleToInvokeId, bundleToInvoke};
			out.writeUTF(bundle.ip_port +" ack");
			writeNotificationLog.invoke(null, new Object[]{bundle.invocationId,notifierDetails, notifieeDetails, "consent", granted });
			return granted;
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}
	public static void sendNotice(MComEntity bundle, String contractName,
			String contractToInvoke, Document contractToInvokeBundleDescriptor){
		String[] header;
		String[] notifieeDetails;
		String bundleToInvoke = getStringAttribute(contractToInvokeBundleDescriptor, "BundleName");
		String bundleToInvokeId = getStringAttribute(contractToInvokeBundleDescriptor, "BundleId");
		
		if(bundle.origin_ip_port == null){
			header = bundle.invoker_ip_port.split("__");
			notifieeDetails = new String[]{bundle.invoker_ip_port, contractToInvoke, bundleToInvokeId, bundleToInvoke};
		}
		else{
			header = bundle.origin_ip_port.split("__");
			notifieeDetails = new String[]{bundle.origin_ip_port, contractToInvoke, bundleToInvokeId, bundleToInvoke};
		}
		try(Socket consentSocket = new Socket(header[0], Integer.parseInt(header[1]));
			DataInputStream in = new DataInputStream(consentSocket.getInputStream());
			DataOutputStream out = new DataOutputStream(consentSocket.getOutputStream());
		){
			
			Class sandboxLoggerClass = ClassLoader.getSystemClassLoader().loadClass("mcom.sandbox.SandboxLogger");
			
				
			Method writeNotificationLog = sandboxLoggerClass.getMethod("writeNotificationLog", new Class[]{Long.class, String[].class,  String[].class, String.class, Boolean.class});
			
			StringBuilder message = new StringBuilder();
			message.append("NOTICEHEADER-" + bundle.ip_port);
			message.append(System.getProperty("line.separator"));
			message.append("NOTICEBODY-");
			message.append(contractName + " contract in " + bundle.bundleName + " bundle has shared your information with " + contractToInvoke);
			message.append(" contract in " + bundleToInvoke + " bundle ");
			
			
			out.writeUTF(message.toString()); // UTF is a string encoding;
			consentSocket.close();
			String[] notifierDetails = new String[]{bundle.ip_port, contractName, String.valueOf(bundle.bundleId), bundle.bundleName};
			
			writeNotificationLog.invoke(null, new Object[]{bundle.invocationId, notifierDetails, notifieeDetails, "notice", true });
			
		}catch(Exception ex){
			ex.printStackTrace();
			
		}
	}
	
	
	public static void sendInvokeeNotice(MComEntity bundle, String contractName,
			String contractToInvoke, Document contractToInvokeBundleDescriptor){

		String bundleToInvoke = getStringAttribute(contractToInvokeBundleDescriptor, "BundleName");
		String bundleToInvokeId = getStringAttribute(contractToInvokeBundleDescriptor, "BundleId");
		String bundleHostAddress = getStringAttribute(contractToInvokeBundleDescriptor, "HostAddress");
		String bundleHostPort = getStringAttribute(contractToInvokeBundleDescriptor, "HostPort");
		String bundleHostNotificationPort = getStringAttribute(contractToInvokeBundleDescriptor, "HostNotificationPort");
		try(Socket consentSocket = new Socket(bundleHostAddress, Integer.parseInt(bundleHostNotificationPort));
				DataInputStream in = new DataInputStream(consentSocket.getInputStream());
				DataOutputStream out = new DataOutputStream(consentSocket.getOutputStream());
			){
				
				Class sandboxLoggerClass = ClassLoader.getSystemClassLoader().loadClass("mcom.sandbox.SandboxLogger");
				
				// Long invocationID, String[] notifier, String[] notifiee, String notifierType, Boolean granted
				
				Method writeNotificationLog = sandboxLoggerClass.getMethod("writeNotificationLog", new Class[]{ Long.class, String[].class,  String[].class, String.class, Boolean.class});
				
				StringBuilder message = new StringBuilder();
				message.append("NOTICEHEADER-" + bundle.ip_port);
				message.append(System.getProperty("line.separator"));
				message.append("NOTICEBODY-");
				message.append(contractName + " contract in " + bundle.bundleName + " bundle has sent your result with  " + bundle.invoker_ip_port);
				
				
				out.writeUTF(message.toString()); // UTF is a string encoding;
				
				
				String[] notifierDetails = new String[]{bundle.ip_port, contractName, String.valueOf(bundle.bundleId), bundle.bundleName};
				String[] notifieeDetails = new String[]{bundleHostAddress + "__" + bundleHostPort, contractToInvoke, bundleToInvokeId, bundleToInvoke};
				
				writeNotificationLog.invoke(null, new Object[]{bundle.invocationId,notifierDetails, notifieeDetails, "notice", true });
				
			}catch(Exception ex){
				ex.printStackTrace();
				
			}
	}
	
	public static SensitivityLevels getSensitivityLevelFromString(String sensitivityLevel){
		
		SensitivityLevels sLevel;
		switch(sensitivityLevel){
			case "PRIVATE":
				sLevel =  SensitivityLevels.PRIVATE;
				break;
			case "SEMI_PRIVATE":
				sLevel =  SensitivityLevels.SEMI_PRIVATE;
				break;
			case "PUBLIC":
				sLevel = SensitivityLevels.PUBLIC;
				break;
			default:
				sLevel = SensitivityLevels.PUBLIC;
			
			
		}
		
		return sLevel;
	}
}
