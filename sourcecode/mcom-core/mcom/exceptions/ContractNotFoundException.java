package mcom.exceptions;

public class ContractNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ContractNotFoundException() { 
		super(); 
	}
	
	public ContractNotFoundException(String message) {
		super(message); 
	}
	
	public ContractNotFoundException(String message, Throwable cause) {
		super(message, cause); 
	}
	
	public ContractNotFoundException(Throwable cause) { 
		super(cause); 
	}
}
