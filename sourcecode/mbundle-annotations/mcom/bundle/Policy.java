package mcom.bundle;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Policy {
	
	
	private boolean consent;
	private boolean notice;
	
	public boolean isConsent() {
		return consent;
	}

	public void setConsent(boolean consent) {
		this.consent = consent;
	}

	public boolean isNotice() {
		return notice;
	}

	public void setNotice(boolean notice) {
		this.notice = notice;
	}
	
	public Document encodeAsXml(){
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;
		try {
			docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("BundlePolicy");
			doc.appendChild(rootElement);
			Element consent = doc.createElement("Consent");
			Element notice = doc.createElement("Notice");
			
			consent.appendChild(doc.createTextNode(""+isConsent()));
			notice.appendChild(doc.createTextNode(""+isNotice()));
			rootElement.appendChild(consent);
			rootElement.appendChild(notice);
			return doc;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return null;
	}
	
}
