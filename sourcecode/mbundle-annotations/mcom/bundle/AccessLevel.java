package mcom.bundle;

public class AccessLevel {
	
	public static final int ALL = 0;
	public static final int SUBNET = 1;
	public static final int IP = 2;
	
	public static final int OTHER = -1;
	
	public static String getType(int type){
		String t = "UNKNOWN";
		
		switch(type){
		case 0:
			t = "ALL";
			break;
		case 1:
			t = "SUBNET";
			break;
		case 2:
			t = "IP";
			break;
		
		}		
		return t;
	}
}
