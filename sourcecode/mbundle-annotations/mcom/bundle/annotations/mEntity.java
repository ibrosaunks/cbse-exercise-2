package mcom.bundle.annotations;
/**
 *  @Author Inah Omoronyia School of Computing Science, University of Glasgow 
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import mcom.bundle.SessionType;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface mEntity {
	int sessionType() default SessionType.STATELESS ;
}
