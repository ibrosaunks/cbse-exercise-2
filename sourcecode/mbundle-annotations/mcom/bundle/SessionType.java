package mcom.bundle;

public class SessionType {
	
	public static final int STATELESS = 0;
	public static final int STATEFUL = 1;
	public static final int PERSISTENT = 2;
	
	public static final int OTHER = -1;
	
	public static String getType(int type){
		String t = "UNKNOWN";
		
		switch(type){
		case 0:
			t = "STATELESS";
			break;
		case 1:
			t = "STATEFUL";
			break;
		case 2:
			t = "PERSISTENT";
			break;
		case -1:
			t = "OTHER";
			break;
		}		
		return t;
	}
}
