package tBundle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mcom.bundle.ContractType;
import mcom.bundle.SensitivityLevels;
import mcom.bundle.annotations.mController;
import mcom.bundle.annotations.mControllerInit;
import mcom.bundle.annotations.mEntity;
import mcom.bundle.annotations.mEntityContract;
import mcom.bundle.annotations.mSocket;
import mcom.bundle.SessionType;
import mcom.entity.MComEntity;

@mController(bundleName = "tBundle.jar")
@mEntity(sessionType=SessionType.STATEFUL)
public class Main extends MComEntity{
	
	public Main(){
		System.out.println("Object Initialised");
		if (map == null) {
			map = new HashMap <String, ArrayList<String> >();
		}
		
	}
	
	@mSocket
	public static String ip_port=null;
	
	public static Map<String, ArrayList<String> > map;
	
	public static void main(String[] args){
		System.out.print("Your input: ");
		for (String arg: args){
			System.out.print(arg + ", ");
		}
	}
	
	static final String desc = "Enter a string";
	
	@mEntityContract(description= desc, contractType = ContractType.GET, sensitivityLevel = SensitivityLevels.PUBLIC)	
	@mControllerInit
	public static String storeToList(String s){
		if (map == null) {
			map = new HashMap <String, ArrayList<String> >();
		}
		
		System.out.println(ip_port);
		String output = "";
		if (!map.containsKey(ip_port)){
			map.put(ip_port, new ArrayList<String>());
		}
		
		ArrayList<String> list = map.get(ip_port);
		list.add(s);
		
		map.replace(ip_port, list);
		
		output = "Your input so far: [\"";
		for (String str: list){
			output += str +"\", \"";
		}
		output = output.substring(0,output.length()-3);
		output +="]";

		return output;
	}

}
