package mcom.kernel.util;
/**
 * @Author Inah Omoronyia School of Computing Science, University of Glasgow 
 */

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.sun.org.apache.xerces.internal.dom.DeepNodeListImpl;

import mcom.bundle.AccessLevel;
import mcom.bundle.Contract;
import mcom.bundle.Policy;
import mcom.bundle.SensitivityLevels;
import mcom.bundle.SessionType;
import mcom.bundle.annotations.mControllerInit;
import mcom.bundle.annotations.mEntity;
import mcom.bundle.annotations.mEntityContract;
import mcom.bundle.util.bMethod;
import mcom.init.Initialiser;
import mcom.kernel.impl.Parameter;
import mcom.kernel.impl.StubImpl;
import mcom.kernel.processor.BundleClassLoader;
import mcom.kernel.processor.BundleDescriptor;
import mcom.kernel.processor.BundleDirProcessor;
import mcom.kernel.processor.BundleJarProcessor;
import mcom.sandbox.logs.InterContractInvocationLog;
import mcom.sandbox.logs.InvocationResult;
import mcom.sandbox.policy.BundlePolicy;
import mcom.wire.impl.ReceiverImpl;
//import mcom.policy.Policy;
import mcom.wire.util.IPResolver;
import mcom.wire.util.RemoteLookupService;

public class KernelUtil {
	
	public static void storeXML(Object bd, String fileDirectory, String bundleName){
		Document doc = null;
		
		if(fileDirectory.equalsIgnoreCase(KernelConstants.BUNDLEDESCRIPTORDIR)){
			
			doc = ((BundleDescriptor) bd).encodeasxml();
		}
		else if(fileDirectory.equalsIgnoreCase(KernelConstants.BUNDLEPOLICYDIR)){
			if(bd instanceof BundleDescriptor)
				doc = ((BundleDescriptor)bd).generatePolicyXml();
			else if(bd instanceof Policy)
				doc = ((Policy) bd).encodeAsXml();
		}
		// write the content into mcom BundleDescDir
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);			
			//StreamResult result = new StreamResult(System.out);	
			File file = new File(fileDirectory +"/"+bundleName.split(".jar")[0]+".xml");
			if(file.exists()){
				file.delete();
				try {
					file.createNewFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			StreamResult result = new StreamResult(file);
			transformer.transform(source, result);
			
			
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}
	
	
	public static Document decodeTextToXml(String dtText){
		Document doc = null;
		try (ByteArrayInputStream stream = new ByteArrayInputStream(dtText.getBytes("utf-8"))){
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			
			//doc = dBuilder.parse(dtText);
			doc = dBuilder.parse(new InputSource(stream));
			doc.getDocumentElement().normalize();
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return doc;
	}
	
	public static int retrieveBundleId(Document doc){
		String bundleId = doc.getElementsByTagName("BundleId").item(0).getTextContent();
		
		return new Integer(bundleId.trim());
	}
	public static String retrieveBundleName(Document doc){
		String bundleName = doc.getElementsByTagName("BundleName").item(0).getTextContent();
		
		return bundleName;
	}
	public static ArrayList<String> retrieveContractNames(Document doc){
		ArrayList<String> cnames = new ArrayList<String>();
		//System.out.println(getBDString(doc));
		
		NodeList nList = doc.getElementsByTagName("Contract");
		for (int temp = 0; temp < nList.getLength(); temp++) {
			
			Node nNode = nList.item(temp);			 
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				//TODO: BUG FIX - prevent multiple contracts with the same contractName in a bundle (See:BundleAnnotationProcessor)
				String sBundleEntityContract = eElement.getElementsByTagName("BundleEntityContract").item(0).getTextContent();
				cnames.add(sBundleEntityContract);				
			}
	 	}
		
		return cnames;
	}
	
	public static HashMap<String, Parameter> retrieveParameters(Document doc, String contractName) {
		HashMap<String, Parameter> parameters = new HashMap<String,Parameter>();
		
		NodeList nList = doc.getElementsByTagName("Contract");
		for (int temp = 0; temp < nList.getLength(); temp++) {			
			Node nNode = nList.item(temp);			 
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				//TODO: BUG FIX - prevent multiple contracts with the same contractName in a bundle (See:BundleAnnotationProcessor)
				String sBundleEntityContract = eElement.getElementsByTagName("BundleEntityContract").item(0).getTextContent();
				
				if(sBundleEntityContract.equals(contractName))	{
					NodeList npList = (NodeList) eElement.getElementsByTagName("Parameters");
					//System.out.println(npList);
					
					//NodeList params = npList.getElementsByTagName("Parameter");
					
							
						
							NodeList epElement = (NodeList) npList.item(0);
							
								for(int temp2 = 0; temp2 < epElement.getLength(); temp2++){
									Node paramf = epElement.item(temp2);
									if (paramf.getNodeType() == Node.ELEMENT_NODE) {
										Element epParam = (Element) paramf;
										String pName = epParam.getElementsByTagName("ClassName").item(0).getTextContent();
										String pValue = epParam.getElementsByTagName("Value").item(0).getTextContent();
								
										String pParamName = epParam.getElementsByTagName("ParamName").item(0).getTextContent();
										parameters.put(pParamName, new Parameter(pName, pValue));
									}
								}
						
										
					break;
				}
			}
	 	}		
		return parameters;
	}
	
	public static String[] retriveRequiredContracts(Document doc, String contractName){
		NodeList nList = doc.getElementsByTagName("Contract");
		String[] contractsRequired = null;
		for(int temp = 0; temp < nList.getLength(); temp++){
			Node nNode = nList.item(temp);
			Element eElement = (Element) nNode;
			//TODO: BUG FIX - prevent multiple contracts with the same contractName in a bundle (See:BundleAnnotationProcessor)
			String sBundleEntityContract = eElement.getElementsByTagName("BundleEntityContract").item(0).getTextContent();
			
			if(sBundleEntityContract.equals(contractName))	{
				String requiredContracts = eElement.getElementsByTagName("RequiredContracts").item(0).getTextContent();
				if(requiredContracts.equalsIgnoreCase("none")){
					return null;
				}else
					contractsRequired = requiredContracts.split(",");
			}
			
			
		}
		return contractsRequired;
	}
	@SuppressWarnings("rawtypes")
	public static BundleDescriptor loadBundleDescriptor(String bundleId) {
		BundleDescriptor bd = null;
		File folder = new File(KernelConstants.BUNDLEDESCRIPTORDIR);
		File[] listOfFiles = folder.listFiles();

		for(File file: listOfFiles){
			try (InputStream is = new FileInputStream(file);
					Reader reader = new InputStreamReader(is, "UTF-8");) {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				
				//InputStream is = checkForUtf8BOMAndDiscardIfAny(new FileInputStream(file));
				
				InputSource source = new InputSource(reader);
				Document doc = dBuilder.parse(source);
				
				doc.getDocumentElement().normalize();


				String BundleId = doc.getElementsByTagName("BundleId").item(0).getTextContent();
				boolean found = false;
				
				if(BundleId.equals(bundleId)){
					found = true;
					bd = new BundleDescriptor();				

					String bundleName = doc.getElementsByTagName("BundleName").item(0).getTextContent();
					bd.setBundleName(bundleName);
					
					bd.setBundleId(new Integer(BundleId));
							
					String hostAddress = doc.getElementsByTagName("HostAddress").item(0).getTextContent();
					InetAddress inaddress = IPResolver.getAddress(hostAddress);
					bd.setAddress(inaddress);
					
					String hostPort = doc.getElementsByTagName("HostPort").item(0).getTextContent();
					bd.setPort(new Integer(hostPort));
					
					String sbundleController = doc.getElementsByTagName("BundleController").item(0).getTextContent();
					Class bundleController = getmClass(sbundleController);
					bd.setBundleController(bundleController);
					
					String sbundleControllerInit = doc.getElementsByTagName("BundleControllerInit").item(0).getTextContent();
					bMethod bundleControllerInit = getbControllerInit(bundleController,sbundleControllerInit);
					bd.setBundleControllerInit(bundleControllerInit);
					
					boolean isSandbox = Boolean.parseBoolean(doc.getElementsByTagName("IsSandboxed").item(0).getTextContent());
					
					bd.setSandbox(isSandbox);
					
					NodeList nList = doc.getElementsByTagName("Contracts");
					for (int temp = 0; temp < nList.getLength(); temp++) {
						
						Node nNode = nList.item(temp);			 
						if (nNode.getNodeType() == Node.ELEMENT_NODE) {
							Element eElement = (Element) nNode;
							String sbundleEntity = eElement.getElementsByTagName("BundleEntity").item(0).getTextContent();
							Class bundleEntity = getmClass(sbundleEntity);
							
							ArrayList<Contract> contracts = getbBundleEntityContracts(bundleEntity);
							for(Contract contract:contracts){
								bd.addContract(contract);
							}
						}
				 	}
				}
				
				if(found){
					break;
				}
				
			} 
			catch (IOException i) {
				i.printStackTrace();
			} 
			catch (ParserConfigurationException e) {
				e.printStackTrace();
			} 
			catch(SAXParseException e){
				//
			}
			catch (SAXException e) {
				e.printStackTrace();
			}	
			
		}
		return bd;
	}
	
	public static void deleteBundleDescriptor(String bundleName){
		String fileName = KernelConstants.BUNDLEDESCRIPTORDIR + "/" + bundleName.split(".jar")[0] + ".xml" ;
		//System.out.println("trying to delete file " + fileName );
		try{
			File descriptor = new File(fileName);
			//System.out.println("file absolute path: " + descriptor.getAbsolutePath());
			if(descriptor.exists())
				descriptor.delete();
			else{
				System.out.println("doesn't exist");
				
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static BundleDescriptor[] loadBundleDescriptors() {
		BundleDescriptor [] bds = new BundleDescriptor[0];		
		File folder = new File(KernelConstants.BUNDLEDESCRIPTORDIR);
		File[] listOfFiles = folder.listFiles();

		for(File file: listOfFiles){
			try (InputStream is = new FileInputStream(file);
					Reader reader = new InputStreamReader(is, "UTF-8");
				) {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				
				//InputStream is = checkForUtf8BOMAndDiscardIfAny(new FileInputStream(file));
				InputSource source = new InputSource(reader);
				Document doc = dBuilder.parse(source);
				
				doc.getDocumentElement().normalize();

				BundleDescriptor bd = new BundleDescriptor();				
				
				String bundleName = doc.getElementsByTagName("BundleName").item(0).getTextContent();
				bd.setBundleName(bundleName);
				
				String BundleId = doc.getElementsByTagName("BundleId").item(0).getTextContent();
				bd.setBundleId(new Integer(BundleId));
						
				String hostAddress = doc.getElementsByTagName("HostAddress").item(0).getTextContent();
				InetAddress inaddress = IPResolver.getAddress(hostAddress);
				bd.setAddress(inaddress);
				
				String hostPort = doc.getElementsByTagName("HostPort").item(0).getTextContent();
				bd.setPort(new Integer(hostPort));
				
				String hostNotificationPort = doc.getElementsByTagName("HostNotificationPort").item(0).getTextContent();
				bd.setNotificationPort(new Integer(hostNotificationPort));
				
				boolean sandboxed = Boolean.parseBoolean(doc.getElementsByTagName("IsSandboxed").item(0).getTextContent());
				bd.setSandbox(sandboxed); 
				
				String sbundleController = doc.getElementsByTagName("BundleController").item(0).getTextContent();
				Class bundleController = getmClass(sbundleController);
				bd.setBundleController(bundleController);
				if(bundleController == null){
					file.delete();
					continue;
				}
				String sbundleControllerInit = doc.getElementsByTagName("BundleControllerInit").item(0).getTextContent();
				bMethod bundleControllerInit = getbControllerInit(bundleController,sbundleControllerInit);
				bd.setBundleControllerInit(bundleControllerInit);
				
				NodeList nList = doc.getElementsByTagName("Contracts");
				for (int temp = 0; temp < nList.getLength(); temp++) {
					
					Node nNode = nList.item(temp);			 
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						String sbundleEntity = eElement.getElementsByTagName("BundleEntity").item(0).getTextContent();
						Class bundleEntity = getmClass(sbundleEntity);
						
						ArrayList<Contract> contracts = getbBundleEntityContracts(bundleEntity);
						for(Contract contract:contracts){
							bd.addContract(contract);
						}
					}
			 	}
				
				BundleDescriptor [] p_temp = new BundleDescriptor[bds.length +1];
				List<BundleDescriptor> p_t = new LinkedList<BundleDescriptor>();
				
				for(BundleDescriptor pt: bds){
					if(pt !=null){
						p_t.add(pt);
					}
				}
				
				int i = 0;
				for(BundleDescriptor pt: p_t){
					p_temp[i] = pt;
					i = i +1;
				}
				
				p_temp[i] = bd;
				bds = p_temp;
				
			} 
			catch (IOException i) {
				i.printStackTrace();
			} 
			catch (ParserConfigurationException e) {
				e.printStackTrace();
			} 
			catch(SAXParseException e){
				//
			}
			catch (SAXException e) {
				e.printStackTrace();
			}	
			
		}
		return bds;
	}
	
	
	@SuppressWarnings("rawtypes")
	private static Class getmClass(String sClass){
		Class mClass = null;
		if(Initialiser.isControllerExists(sClass)){
			mClass = Initialiser.getController(sClass);
		}else{
			File[] mBundles = BundleDirProcessor.loadFilesInBundleDirectory();//list of mBundles (files) in BundleDir
				
			for(File mBundle:mBundles){ 				
				String [] mBundleClassFiles = new BundleJarProcessor(mBundle).getClassFiles();	//list of classes in mBundle					
				
				boolean found = false;
				//create custom class loader
				try{
					URLClassLoader loader = (URLClassLoader)ClassLoader.getSystemClassLoader();
					BundleClassLoader mBundleClassLoader = new BundleClassLoader(loader.getURLs());
					String path = mBundle.getAbsolutePath();
					path = "jar:file://"+ path+"!/";
					mBundleClassLoader.addURL(mBundle.toURI().toURL());		
					for(String cf: mBundleClassFiles){
						Class oClass = Class.forName(cf, true, mBundleClassLoader);
						if(oClass.getName().equals(sClass)){
							mClass = oClass;
							found = true;
							// save the controller
							Initialiser.addController(sClass, mClass);
							break;
						}
											
					}	
					if(found){
						break;
					}
				}
				catch (ClassNotFoundException e) {}
				catch(NoClassDefFoundError e) {
					//handle carefully
				}
				catch(UnsatisfiedLinkError e) {
					//handle carefully
				} 
				catch (MalformedURLException e) {
					e.printStackTrace();
				}
				finally{}			
			}
		}
		
		return mClass;		
	}
		
	@SuppressWarnings("rawtypes")
	private static bMethod getbControllerInit(Class oClass, String sMethod){
		bMethod bundleControllerInit = null;
		boolean found = false;
		//create custom class loader
		try{				
			Method[] methods = oClass.getMethods();
			for(Method m:methods){
				Annotation[] mannotations = m.getAnnotations();
				for(Annotation annotation2: mannotations){
					if(annotation2 instanceof mControllerInit){
						if(m.getName().equals(sMethod)){
							bundleControllerInit = bMethod.encodeAsbMethod(m, oClass);
							found = true;
							break;
						}								
					}
				}
				if(found){
					break;
				}
			}	
		}
		catch(NoClassDefFoundError e) {
			//handle carefully
		}
		catch(UnsatisfiedLinkError e) {
			//handle carefully
		}			
		finally{}
		
		return bundleControllerInit;		
	}
	
	@SuppressWarnings("rawtypes")
	private static ArrayList<Contract> getbBundleEntityContracts(Class oClass){
		ArrayList<Contract> contracts = new ArrayList<Contract>();
		
		//create custom class loader
		int sessionType = 0;
		Annotation[] cannotations = oClass.getAnnotations();
		for(Annotation cannotation: cannotations){
			if(cannotation instanceof mEntity){
				
				sessionType = ((mEntity) cannotation).sessionType();
			}
		}
		try{
			Method[] methods = oClass.getMethods();
			for(Method m:methods){
				Annotation[] mannotations = m.getAnnotations();
				for(Annotation annotation2: mannotations){
					if(annotation2 instanceof mEntityContract){
						Contract contract = new Contract();							
						contract.setBundleEntity(oClass);
						contract.setBundleEntityContract(bMethod.encodeAsbMethod(m, oClass));	
						String description = ((mEntityContract) annotation2).description();
						contract.setDescription(description);
						contract.setRequiredContracts(((mEntityContract) annotation2).requiredContracts());
						int contractType = ((mEntityContract) annotation2).contractType();
						contract.setContractType(contractType);
						contract.setReturnType(m.getReturnType().getName());
						String accessLevel = AccessLevel.getType(((mEntityContract) annotation2).accessLevel());
						contract.setContractAccessLevel(accessLevel);
						contract.setSensitivityLevel(((mEntityContract)annotation2).sensitivityLevel());

						contract.setBundleEntitySessionType(SessionType.getType(sessionType));
						contracts.add(contract);	
					}
				}
			}	
		}
		catch(NoClassDefFoundError e) {
			//handle carefully
		}
		catch(UnsatisfiedLinkError e) {
			//handle carefully
		}			
		finally{}
		
		return contracts;		
	}
	
	@SuppressWarnings("rawtypes")
	public static Document encodeRemoteCallAsxml(String bhip, int bhport, int bid, String cName, HashMap<String,Parameter> parameters, Policy policy, String origin){
		Document doc = null;
		
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			// root elements
			doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("InvokeRequest");
			doc.appendChild(rootElement);
			
			Element bhhostIp = doc.createElement("BundleHostIP");
			bhhostIp.appendChild(doc.createTextNode(bhip));				
			rootElement.appendChild(bhhostIp);

			Element bhhostPort = doc.createElement("BundleHostPort");
			bhhostPort.appendChild(doc.createTextNode(""+bhport));				
			rootElement.appendChild(bhhostPort);
			if(origin != null){
				Element xorigin = doc.createElement("PrivateDataOrigin");
				xorigin.appendChild(doc.createTextNode(origin));				
				rootElement.appendChild(xorigin);
			}
			Element notificationPort = doc.createElement("NotificationPort");
			notificationPort.appendChild(doc.createTextNode(""+ReceiverImpl.notifyListenSocket.getLocalPort()));
			rootElement.appendChild(notificationPort);
			
			Element bundleId = doc.createElement("BundleId");
			bundleId.appendChild(doc.createTextNode(""+bid));
			rootElement.appendChild(bundleId);
			
			Element contract = doc.createElement("Contract");
			rootElement.appendChild(contract);
			Element xpolicy = doc.createElement("Policy");
			if(policy != null){
		    	Element consent = doc.createElement("Consent");
		    	Element notice = doc.createElement("Notice");
		    	consent.appendChild(doc.createTextNode(policy.isConsent() + ""));
		    	notice.appendChild(doc.createTextNode(policy.isNotice() + ""));
		    	xpolicy.appendChild(consent);
		    	xpolicy.appendChild(notice);
		    }
			rootElement.appendChild(xpolicy);
			Element contractName = doc.createElement("ContractName");
			contractName.appendChild(doc.createTextNode(""+cName));
			contract.appendChild(contractName);
			
			Element xparameters= doc.createElement("Parameters");
			
			Iterator it = parameters.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pairs = (Map.Entry)it.next();
		        //String pName = (String)pairs.getKey();
		        Parameter pValue = (Parameter)pairs.getValue();
		        
		        Element xparameter= doc.createElement("Parameter");
				
				Element xname = doc.createElement("Type");
				xname.appendChild(doc.createTextNode(pValue.getClassName()));
				xparameter.appendChild(xname);
				
				Element xvalue = doc.createElement("Value");
				xvalue.appendChild(doc.createTextNode(pValue.getValue()));
				xparameter.appendChild(xvalue);
				
				xparameters.appendChild(xparameter);
		    } 
		    contract.appendChild(xparameters);
		    contract.appendChild(xpolicy);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} 
		
		return doc;
	}	
	
	public static Document loadPolicy(String bundleName){
		File folder = new File(KernelConstants.BUNDLEPOLICYDIR);
		File[] listOfFiles = folder.listFiles();
		for(File file: listOfFiles){
			if(file.getName().split(".xml")[0].equalsIgnoreCase(bundleName.split(".jar")[0])){
				
				try(InputStream is = new FileInputStream(file);
					Reader reader = new InputStreamReader(is, "UTF-8");){
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				
				//InputStream is = checkForUtf8BOMAndDiscardIfAny(new FileInputStream(file));
					
					InputSource source = new InputSource(reader);
					Document doc = dBuilder.parse(source);
				
					doc.getDocumentElement().normalize();
					
					return doc;
					
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public static boolean getSandboxStatusByBundleName(String bundleName){
		if(bundleName.contains(".jar"))
			bundleName = bundleName.split(".jar")[0];
		
		File folder = new File(KernelConstants.BUNDLEDESCRIPTORDIR);
		File[] listOfFiles = folder.listFiles();
		for(File file: listOfFiles){
			if(file.getName().split(".xml")[0].equalsIgnoreCase(bundleName)){
				
				try (InputStream is = new FileInputStream(file);
					Reader reader = new InputStreamReader(is, "UTF-8");
						
					){
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				
				//InputStream is = checkForUtf8BOMAndDiscardIfAny(new FileInputStream(file));
					InputSource source = new InputSource(reader);
					Document doc = dBuilder.parse(source);
				
					doc.getDocumentElement().normalize();
					
					
					
					
					return Boolean.parseBoolean(doc.getElementsByTagName("IsSandboxed").item(0).getTextContent());
					
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
		return true;
	}
	public static String getBDString(Document doc ) {
		String output = "";
		
		try {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			output = writer.getBuffer().toString().replaceAll("\n|\r", "");
			
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}		
		return output;
	}
	

	public static String prettyPrint(final String xml){  

	    if (StringUtils.isBlank(xml)) {
	        throw new RuntimeException("xml was null or blank in prettyPrint()");
	    }

	    final StringWriter sw;

	    try {
	        final org.dom4j.io.OutputFormat format = org.dom4j.io.OutputFormat.createPrettyPrint();
	        final org.dom4j.Document document = DocumentHelper.parseText(xml);
	        sw = new StringWriter();
	        final org.dom4j.io.XMLWriter writer = new org.dom4j.io.XMLWriter(sw, format);
	        writer.write(document);
	    }
	    catch (Exception e) {
	        throw new RuntimeException("Error pretty printing xml:\n" + xml, e);
	    }
	    return sw.toString();
	}
	
	public static void undeploy(int bundleId){
		try{
		System.out.println("Undeploying " + bundleId);
		Iterator iter = RemoteLookupService.getLookupResults().entrySet().iterator();
		Document bd_doc = null;
		while(iter.hasNext()){
			 Map.Entry pairs = (Map.Entry)iter.next();
			 String body = (String)pairs.getValue();
			  
			 bd_doc = KernelUtil.decodeTextToXml(body.trim());
			 
			 if(bundleId == retrieveBundleId(bd_doc)){
				 iter.remove();
				 break;
			 }
		}
		if(bd_doc != null)
			new StubImpl().deAdvertise(retrieveBundleId(bd_doc));
		
			Initialiser.removeBundleDescriptor(loadBundleDescriptor(String.valueOf(retrieveBundleId(bd_doc))));
			deleteBundleDescriptor(retrieveBundleName(bd_doc));
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public static void setConsentPolicy(Document policyDoc, String bundleName, boolean seekConsent){
		//Document doc = loadPolicy(bundleName);
		
		Policy policy = new Policy();
		policy.setNotice(Boolean.parseBoolean(policyDoc.getElementsByTagName("Notice").item(0).getTextContent()));
		policy.setConsent(seekConsent);
		
		storeXML(policy, KernelConstants.BUNDLEPOLICYDIR, bundleName);
	}
	
	public static void setNoticePolicy(Document policyDoc, String bundleName, boolean sendNotice){
		//Document doc = loadPolicy(bundleName);
		
		Policy policy = new Policy();
		policy.setConsent(Boolean.parseBoolean(policyDoc.getElementsByTagName("Consent").item(0).getTextContent()));
		policy.setNotice(sendNotice);
		
		storeXML(policy, KernelConstants.BUNDLEPOLICYDIR, bundleName);
	}
	
	public static SensitivityLevels getSensitivityLevelFromString(String sensitivityLevel){
		
		SensitivityLevels sLevel;
		switch(sensitivityLevel){
			case "PRIVATE":
				sLevel =  SensitivityLevels.PRIVATE;
				break;
			case "SEMI_PRIVATE":
				sLevel =  SensitivityLevels.SEMI_PRIVATE;
				break;
			case "PUBLIC":
				sLevel = SensitivityLevels.PUBLIC;
				break;
			default:
				sLevel = SensitivityLevels.PUBLIC;
			
			
		}
		
		return sLevel;
	}
//	private static InputStream checkForUtf8BOMAndDiscardIfAny(InputStream inputStream) throws IOException {
//	    PushbackInputStream pushbackInputStream = new PushbackInputStream(new BufferedInputStream(inputStream), 3);
//	    byte[] bom = new byte[3];
//	    if (pushbackInputStream.read(bom) != -1) {
//	        if (!(bom[0] == (byte) 0xEF && bom[1] == (byte) 0xBB && bom[2] == (byte) 0xBF)) {
//	            pushbackInputStream.unread(bom);
//	        }
//	    }
//	    return pushbackInputStream; 
//	 }


	public static void addToBundleResults(Integer bundleId,
			InvocationResult result) {
		// TODO Auto-generated method stub
		if(Initialiser.bundleResults.containsKey(bundleId)){
			
			List<InvocationResult> bundleRes = Initialiser.bundleResults.get(bundleId);
			bundleRes.add(result);
			Initialiser.bundleResults.replace(bundleId, bundleRes);
		}else{
			List<InvocationResult> bundleRes = Collections.synchronizedList(new ArrayList<InvocationResult>());
			bundleRes.add(result);
			Initialiser.bundleResults.put(bundleId, bundleRes);
		}
		
	}
	
	public static void addToInterC(Long invocationId, InterContractInvocationLog log){
		if(Initialiser.interContractInvocationLogs.containsKey(invocationId)){
			
			List<InterContractInvocationLog> logs = Initialiser.interContractInvocationLogs.get(invocationId);
			logs.add(log);
			Initialiser.interContractInvocationLogs.replace(invocationId, logs);
		}else{
			List<InterContractInvocationLog> logs = Collections.synchronizedList(new ArrayList<InterContractInvocationLog>());
			logs.add(log);
			Initialiser.interContractInvocationLogs.put(invocationId, logs);
		}
	}
	
	public static Policy extractPolicyFromRemoteCall(Document inv_doc){
		NodeList p = inv_doc.getElementsByTagName("Policy"); 
		for (int temp = 0; temp < p.getLength(); temp++) {	
			Node pn = p.item(temp);
			if(pn.getNodeType() == Node.ELEMENT_NODE){
				Element elem = (Element) pn;
				if(elem.getElementsByTagName("Consent").item(0) != null && elem.getElementsByTagName("Notice").item(0) != null){
					
					boolean consent = Boolean.parseBoolean(elem.getElementsByTagName("Consent").item(0).getTextContent());
					boolean notice = Boolean.parseBoolean(elem.getElementsByTagName("Notice").item(0).getTextContent());
					Policy policy = new Policy();
					policy.setConsent(consent);
					policy.setNotice(notice);
					return policy;
				}
			}
		} return null;
	}
	
	public static String[] retrievePrivateParams(Object[] arguments){
		String privateParams = ""; 
		String[] privateParamsArray;
		for(Object argument: arguments){
			String strArg = argument.toString();
			if(strArg.endsWith("pr")){
				privateParams += strArg.split("\\|")[0].trim() + ",";
			}
		}
		if(!"".equalsIgnoreCase(privateParams)){
	    	privateParams = privateParams.substring(0, privateParams.length());
	    	privateParamsArray = privateParams.split(",");
	    }else
	    	privateParamsArray = new String[0];
		
		return privateParamsArray;
	}
	
	public static String[] retrievePrivateParams(Map<String, Parameter> parameters){
		String privateParams = ""; 
		String[] privateParamsArray;
		Iterator it = parameters.entrySet().iterator();
	    while (it.hasNext()) {
	    	Map.Entry pairs = (Map.Entry)it.next();
		    
		    Parameter pValue = (Parameter)pairs.getValue();
		   
		    if(pValue.getValue().endsWith("pr")){
		    	
		        privateParams += pValue.getValue().split("\\|")[0].trim() + ",";
		    }
		        
	    }
	    if(!"".equalsIgnoreCase(privateParams)){
	    	privateParams = privateParams.substring(0, privateParams.length() - 1);
	    	//System.out.println(privateParams);
	    	privateParamsArray = privateParams.split(",");
	    }else
	    	privateParamsArray = new String[0];
	    
	    
	    return privateParamsArray;
	}
}
