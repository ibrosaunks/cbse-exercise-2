package mcom.kernel.util;
/**
 * @Author Inah Omoronyia School of Computing Science, University of Glasgow 
 */

public class KernelConstants {
	public static final String BUNDLEDIR = "LocalBundleDir";
	public static final String BUNDLEDESCRIPTORDIR = "LocalBundleDescDir";
	public static final String BUNDLEPOLICYDIR = "LocalBundlePolicyDir";
	public static final String LOGDIR = "Logs";
	public static final String INTER_CONTRACT_INVOCATION_LOG_DIR = KernelConstants.LOGDIR+ "/InterContractInvocationLog.ser";
	public static final String CONTRACT_NOTIFICATION_LOG_DIR = KernelConstants.LOGDIR+ "/ContractNotificationLog.ser";
	public static final float UPGRADE_THRESHOLD = 20;
	public static final float VIOLATION_THRESHOLD  = -3;
	
}
