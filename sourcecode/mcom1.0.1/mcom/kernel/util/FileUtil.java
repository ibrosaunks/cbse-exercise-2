package mcom.kernel.util;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;


public class FileUtil {
	
	private static String fileName = "session.ser"; 
     
	
	
	public static void writeSessionToFile(Map<String, Object> persistentObject){
		try {
	        FileOutputStream fileOut = new FileOutputStream(fileName);
	        ObjectOutputStream out = new ObjectOutputStream(fileOut);
	        out.writeObject(persistentObject);
	        out.close();
	        fileOut.close();
	    } catch (IOException ex) {
	    	ex.printStackTrace();
	    }
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String, Object> readSessionFromFile(){
		 Map<String, Object> persistentObject = null;
		    try {
		    	File sessionFile = new File(fileName);
		    	if(!sessionFile.exists())
		    		sessionFile.createNewFile();
		    	if(sessionFile.length() == 0L){
		    		return new HashMap<String, Object>();
		    	}
		        ObjectInputStream in = new ObjectInputStream(new FileInputStream(sessionFile));
		        persistentObject = (Map<String, Object>) in.readObject(); 
		        in.close();
		        System.out.println("persistent object" + persistentObject);
		    }
		    catch(Exception e) {
		    	e.printStackTrace();
		    }
		    return persistentObject;
	}
	/*public void writeToSession(String property, Object value){
		FileInputStream fis = null;
		Properties props = new Properties();
		try {
			fis = new FileInputStream(file);
		        
		   
		    props.load(fis);
		    fis.close();  
			FileOutputStream out = new FileOutputStream(file.getAbsolutePath());
			props.setProperty(property, value.toString());
			props.store(out, null);
			out.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  //clears file every time
		
		
	
	}
	public String getFromSession(String property){
		FileInputStream fis = null;
		String value = null;
		Properties props = new Properties();
		try {
			fis = new FileInputStream(file);
		    props.load(fis);
		    fis.close();  
			
			value = props.getProperty(property);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		return value;
	}*/
	
}
