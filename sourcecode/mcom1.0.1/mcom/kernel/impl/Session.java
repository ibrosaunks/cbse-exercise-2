package mcom.kernel.impl;

import java.util.HashMap;
import java.util.Map;

public class Session {
	private static Map<String, Object> session = new HashMap<String, Object>();;
	
	public static void addToSession(String className, Object object){
		
			if(session.containsKey(className)){
				System.out.println("replacing the value");
				System.out.println("old object: " + session.get(className).toString());
				System.out.println("new object: " + object.toString());
				session.put(className, object);
			} else{
				System.out.println("doesn't contain key man");
				session.put(className, object);
			}
		
			
	}
	
	public static Object getFromSession(String className){
		if(session.containsKey(className)) return session.get(className); else return null;
	}
}
