package mcom.kernel.impl;

public class Parameter {
	private String className;
	
	public Parameter(String className, String value) {
		
		this.className = className;
		this.value = value;
	}

	private String value;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
