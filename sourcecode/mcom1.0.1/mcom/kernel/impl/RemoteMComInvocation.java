package mcom.kernel.impl;

import java.io.File;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import mcom.bundle.SensitivityLevels;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.w3c.dom.Document;
//import org.w3c.dom.Element;
import org.w3c.dom.NodeList;














import mcom.bundle.Contract;
import mcom.bundle.Policy;
import mcom.bundle.SessionType;
//import mcom.bundle.annotations.mSocket;
import mcom.init.Initialiser;
import mcom.kernel.processor.BundleDescriptor;
import mcom.kernel.util.KernelUtil;
import mcom.sandbox.SandboxLogger;
import mcom.sandbox.logs.InvocationResult;
import mcom.wire.impl.ReceiverImpl;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class RemoteMComInvocation {
	
	/**
	 * 
	 * 
	 * This method takes as parameter a text encoding for the remote invocation. 
	 * It parses the Document object, retrieving the details of the required bundle and contract
	 * It then Loads the bundle entity object either from session or creates a new instance
	 * It retrieves all the parameter classes and does a reflective method invocation
	 * It returns the result of the invocation
	 * 
	 * @param inv_doc invocation xml Document Object
	 * @return Object result object
	 */
	
	
	public static InvocationResult executeLocalCall(Long invocationId, int bundleId, String contractName,	HashMap<String, Parameter> parameters, Policy policy, String origin) throws Exception{
		try{
			BundleDescriptor bd = KernelUtil.loadBundleDescriptor(String.valueOf(bundleId));
			Class<?> bundleClass = bd.getBundleController();
			Object classInstance = null;
			SensitivityLevels contractSensitivityLevel = SensitivityLevels.PUBLIC ;	
			for(Contract con : bd.getContracts()){
				if(con.getBundleEntityContract().getMethodName().equalsIgnoreCase(contractName)){
				
					bundleClass = con.getBundleEntity();
					contractSensitivityLevel = con.getSensitivityLevel();
				}
			}
			int parametersLength = parameters.size();
			classInstance = bundleClass.newInstance();
			
			setContractClassVariables(classInstance, Initialiser.local_address.getHostAddress()+ "__" + ReceiverImpl.listenSocket.getLocalPort(),Initialiser.local_address.getHostAddress()+ "__" + ReceiverImpl.notifyListenSocket.getLocalPort(),
					bd.getBundleName(), bd.getBundleId(), invocationId, origin, policy);
		

			Iterator<?> evit = parameters.entrySet().iterator();
			int i = 0;
			Object[] arguments = new Object[parametersLength];
			Class[] parameterTypes = new Class[parametersLength];
			while (evit.hasNext()) {
				Map.Entry evpairs = (Map.Entry)evit.next();
				Parameter p = (Parameter) evpairs.getValue();
				
				parameterTypes[i] = ClassLoader.getSystemClassLoader().loadClass(p.getClassName());
				arguments[i] = p.getValue();
				i++;
			}
		
			Method methodToInvoke = bundleClass.getMethod(contractName, parameterTypes);
			Object result = methodToInvoke.invoke(classInstance, arguments);
			
			//  SandboxLogger.logContractInvocation(bundleId, bd.getBundleName(), parameters, invocationId ,Initialiser.local_address.getHostAddress()+ "__" + ReceiverImpl.notifyListenSocket.getLocalPort());
			return new InvocationResult(String.valueOf(result), contractSensitivityLevel);
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			return null;
		}
	}
	public static InvocationResult executeRemoteCall(Document inv_doc, String ip_port, Long invocationId, String origin, Policy policy){
		//System.out.println(KernelUtil.prettyPrint(KernelUtil.getBDString(inv_doc))); //uncomment to study encoded call content		
		
		Object result = null; 
		SensitivityLevels contractSensitivityLevel = SensitivityLevels.PUBLIC ;	
		
		String bundleId = inv_doc.getElementsByTagName("BundleId").item(0).getTextContent();
		//Long invocationID = Long.parseLong(inv_doc.getElementsByTagName("InvocationId").item(0).getTextContent());
		BundleDescriptor bd = KernelUtil.loadBundleDescriptor(bundleId);
		String method = inv_doc.getElementsByTagName("Contract").item(0).getChildNodes().item(0).getTextContent();
		
		String notificationPort = inv_doc.getElementsByTagName("NotificationPort").item(0).getTextContent();
		//System.out.println(inv_doc.getElementsByTagName("Contract").item(0).getChildNodes().item(0).getTextContent());
		
		String notificationAddress = ip_port.split("__")[0] + "__" + notificationPort;
		NodeList parameters = inv_doc.getElementsByTagName("Contract").item(0).getChildNodes().item(1).getChildNodes();
		//String sessionType = null;
		int parametersLength = parameters.getLength();
		Object[] arguments = new Object[parametersLength];
		Class[] parameterTypes = new Class[parametersLength];
		
		
		
		try {
			Class bundleClass = bd.getBundleController();
			Object classInstance = null;
			
			for(Contract con : bd.getContracts()){
				if(con.getBundleEntityContract().getMethodName().equalsIgnoreCase(method)){
					//sessionType= con.getBundleEntitySessionType();
					bundleClass = con.getBundleEntity();
					contractSensitivityLevel = con.getSensitivityLevel();
				}
			}
			
			//classInstance = returnContractClassObject(sessionType, bundleClass);
			//Long invocationId = Initialiser.getContractInvocationID();
			classInstance = bundleClass.newInstance();
			setContractClassVariables(classInstance, ip_port, notificationAddress, bd.getBundleName(), new Integer( bundleId), invocationId, origin, policy);
			
			for(int i=0; i<parametersLength;i++){
				
				parameterTypes[i] = ClassLoader.getSystemClassLoader().loadClass(parameters.item(i).getChildNodes().item(0).getTextContent());
				arguments[i] = parameters.item(i).getChildNodes().item(1).getTextContent();
			}
			
			
			
			Method methodToInvoke = bundleClass.getMethod(method, parameterTypes);
			
			result = methodToInvoke.invoke(classInstance, arguments);
			//  SandboxLogger.logContractInvocation(Integer.parseInt(bundleId), bd.getBundleName(), arguments, invocationId,  notificationAddress );
			/*if(sessionType.equalsIgnoreCase(SessionType.getType(SessionType.PERSISTENT)))
				Initialiser.savePersistentSession(bundleClass.getName(), classInstance);*/
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} 
		
		return new InvocationResult(String.valueOf(result), contractSensitivityLevel);
	}
	
	
	/*private static Object returnContractClassObject(String sessionType, Class bundleClass){
		Object classInstance = null;
		try{
			if(sessionType.equalsIgnoreCase(SessionType.getType(SessionType.STATEFUL))){
				if (Initialiser.objectExists(bundleClass)) {
					classInstance = Initialiser.getObject(bundleClass);
			
				}
				else {
					classInstance = bundleClass.newInstance();
					Initialiser.addObject(bundleClass, classInstance);
				}
			}
			else if(sessionType.equalsIgnoreCase(SessionType.getType(SessionType.PERSISTENT))){
				
				
				File ses = new File("sessions/" + bundleClass.getName() + "session.json");
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					
				if(ses.exists()){
					try {
						classInstance = mapper.readValue(ses, bundleClass);
						
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					if(!ses.getParentFile().exists())
						ses.getParentFile().mkdirs();
					ses.createNewFile();
						classInstance = bundleClass.newInstance();
				}
			}else{
				classInstance = bundleClass.newInstance();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return classInstance;
	}*/
	
	private static void setContractClassVariables(Object classInstance, String ip_port, String invoker_ip_port, String bundleName, int bundleId, Long invocationId, String origin, Policy policy){
		
		try{
			classInstance.getClass().getSuperclass().getDeclaredField("bundleId").set(classInstance, bundleId);
			classInstance.getClass().getSuperclass().getDeclaredField("bundleName").set(classInstance, bundleName);
			classInstance.getClass().getSuperclass().getDeclaredField("ip_port").set(classInstance, ip_port);
			classInstance.getClass().getSuperclass().getDeclaredField("invoker_ip_port").set(classInstance, invoker_ip_port);
			
			//Field invocationId = 
			classInstance.getClass().getSuperclass().getDeclaredField("invocationId").set(classInstance, invocationId);
			if(origin != null){
				//System.out.println("Setting origin");
				classInstance.getClass().getSuperclass().getDeclaredField("origin_ip_port").set(classInstance, origin);
			} 
			if(policy != null){
				classInstance.getClass().getSuperclass().getDeclaredField("originPolicy").set(classInstance, policy);

			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	

}
