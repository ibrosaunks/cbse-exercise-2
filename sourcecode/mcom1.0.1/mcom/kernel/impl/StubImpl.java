package mcom.kernel.impl;

/**
 * @Author Inah Omoronyia School of Computing Science, University of Glasgow 
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.w3c.dom.Document;

import mcom.bundle.Policy;
import mcom.console.Display;
import mcom.init.Initialiser;
import mcom.kernel.Stub;
import mcom.kernel.processor.BundleDescriptor;
import mcom.kernel.processor.BundleDescriptorFactory;
import mcom.kernel.util.KernelUtil;


import mcom.sandbox.SandboxLogMonitor;
import mcom.sandbox.SandboxLogger;
import mcom.sandbox.logs.ContractInfo;
import mcom.sandbox.logs.ContractInvocationLog;
import mcom.sandbox.logs.InterContractInvocationLog;
import mcom.sandbox.logs.InvocationResult;
import mcom.wire.impl.ReceiverImpl;
import mcom.wire.impl.SenderImpl;
import mcom.wire.util.DynamicRegistrarDiscovery;
import mcom.wire.util.RemoteLookupService;

public class StubImpl implements Stub{

	/**
	 * Deploys all bundles in the LocalBundleDir folder
	 * 
	 * 
	 * @return  boolean true if all bundles deployed 
	 * 
	 */
	public boolean deploy() {
		try {
			//Long startTime = System.currentTimeMillis();
			BundleDescriptorFactory.buildBundleDescriptors();
			//long elapsedTime = System.currentTimeMillis() - startTime;
			//System.out.println(elapsedTime);
			System.out.println("bundle deployment completed");
			localLookup();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Retrieves a Bundle Descriptor
	 * 
	 * Constructs advertisement message from Bundle Descriptor string and then sends Advertise request to all known Registrars
	 * @return  boolean true if bundle is successfully advertised
	 * 
	 */
	public boolean advertise(int bundleId) {
		//Advertise a local BundleDescriptor with bundleId on known registers
		boolean advertised = false;
		for(BundleDescriptor bd:Initialiser.bundleDescriptors){
			if(bd.getBundleId() == bundleId){
				
				StringBuilder message = new StringBuilder();
				String header = Initialiser.local_address.getHostAddress()+"__"+ReceiverImpl.listenSocket.getLocalPort() +
						"__"+bundleId + "__"+Initialiser.VERSION;
				message.append("ADVERTHEADER-"+header);
				
				message.append(System.getProperty("line.separator"));
				message.append("ADVERTBODY-"+bd.getBDString());
								
				//advertise to all known Registers
				String advert = message.toString();	
				if(DynamicRegistrarDiscovery.getActiveRegistrars() == null || DynamicRegistrarDiscovery.getActiveRegistrars().size() ==0){
					System.err.println("No known Registrar");
				}
				else{
					for(String regip_port:DynamicRegistrarDiscovery.getActiveRegistrars()){
						String [] res = regip_port.split("__");
						String serviceip = res[0];
						String serviceport = res[1];
						new SenderImpl().sendMessage(serviceip, new Integer(serviceport), advert);	

					}
				    advertised = true;	
				}
				//RemoteLookupService.addLookupResult(header, bd.getBDString());
			}
		}
		if(!advertised){
			System.err.println("Advertised: "+advertised);
		}
		else{
			System.out.println("Advertised: "+advertised);
		}
		
		return true;
	}
	public void advertiseAll(){
		for(BundleDescriptor bd:Initialiser.bundleDescriptors){
			
			boolean advertised = false;
			StringBuilder message = new StringBuilder();
			String header = Initialiser.local_address.getHostAddress()+"__"+ReceiverImpl.listenSocket.getLocalPort() +
					"__"+bd.getBundleId() + "__"+Initialiser.VERSION;
			message.append("ADVERTHEADER-"+header);
			
			message.append(System.getProperty("line.separator"));
			message.append("ADVERTBODY-"+bd.getBDString());
								
				//advertise to all known Registers
				String advert = message.toString();	
				if(DynamicRegistrarDiscovery.getActiveRegistrars() == null || DynamicRegistrarDiscovery.getActiveRegistrars().size() ==0){
					System.err.println("No known Registrar");
				}
				else{
					for(String regip_port:DynamicRegistrarDiscovery.getActiveRegistrars()){
						String [] res = regip_port.split("__");
						String serviceip = res[0];
						String serviceport = res[1];
						new SenderImpl().sendMessage(serviceip, new Integer(serviceport), advert);	

					}
				    advertised = true;	
				}
				//RemoteLookupService.addLookupResult(header, bd.getBDString());
				if(!advertised){
					System.err.println("Advertised: "+advertised);
				}
				else{
					System.out.println("Advertised: "+advertised);
				}
			
		}
	}
	public void deAdvertise(int bundleId){
		//boolean deAdvertised
		for(BundleDescriptor bd: Initialiser.bundleDescriptors){
			if(bd.getBundleId() == bundleId){
				StringBuilder message = new StringBuilder();
				message.append("UNADVERTHEADER-"+Initialiser.local_address.getHostAddress()+"__"+ReceiverImpl.listenSocket.getLocalPort());
				message.append("__"+bundleId);
				message.append("__"+Initialiser.VERSION);
				message.append(System.getProperty("line.separator"));
				message.append("UNADVERTBODY-"+bd.getBDString());
				
				String advert = message.toString();	
				if(DynamicRegistrarDiscovery.getActiveRegistrars() == null || DynamicRegistrarDiscovery.getActiveRegistrars().size() ==0){
					break;
				}
				else{
					for(String regip_port:DynamicRegistrarDiscovery.getActiveRegistrars()){
						String [] res = regip_port.split("__");
						String serviceip = res[0];
						String serviceport = res[1];
						new SenderImpl().sendMessage(serviceip, new Integer(serviceport), advert);	

					}
				   
				}
			}
		}
	}
	
	/**
	 * Inspects and returns a string representation of all the bundles deployed locally on an instance
	 * 
	 * 
	 *
	 */
	public void localLookup() {
		BundleDescriptor [] bds = KernelUtil.loadBundleDescriptors();
		System.out.println("No bundles: "+bds.length);
		for(int i =0;i<bds.length;i++){			
			Initialiser.addBundleDescriptor(bds[i]);
			int m = i+1;
			System.out.println("||("+m+") BundleID:"+bds[i].getBundleId()+" BundleName:"+bds[i].getBundleName()+"||");
			System.out.println(bds[i]);			
		}			
	}
		
	/**
	 * Sends lookup request to all known registers 
	 * 
	 * Then prints results of lookup
	 */
	public void remoteLookup() {
		// lookup on all contracts advertised on known Registers
		// (This function is similar to localLookup() beside the requirement that lookup 
		// should be executed on known remote Registers)	
		RemoteLookupService.doRemoteLookup();
	}
	
	/**
	 * Collects from the user BundleId, Contract Name and Contract Parameters
	 * Builds invocation request string and sends request to bundle host
	 * 
	 */

	@SuppressWarnings("rawtypes")
	public void invoke() {
		//Invoke a specific contract from the lookup list	
		int bundleId = -1;
		String contractName = null;
		HashMap<String,Parameter> parameters = new HashMap<String,Parameter>(); //parameter name:value
		
		System.out.println("Input BundleID:");

		String bid = Display.scanner.nextLine();
		if(bid !=null && bid.trim().length()>0){
			if(Display.isNumeric(bid.trim())){
				bundleId = new Integer(bid.trim());
				
				//verify bundleId from
				Map<String, String> lookupResults = RemoteLookupService.getLookupResults();
				if(lookupResults == null || lookupResults.size() ==0){
					System.err.println("Empty rlookup");
					return;
				}
				Iterator it = lookupResults.entrySet().iterator();
				
				boolean bid_exit = false;
				while (it.hasNext()) {
				  Map.Entry pairs = (Map.Entry)it.next();
				  String header = (String)pairs.getKey();
				  String body = (String)pairs.getValue();
				  
				  Document bd_doc = KernelUtil.decodeTextToXml(body.trim());
				    
				  if(bundleId == KernelUtil.retrieveBundleId(bd_doc)){
					  
					  bid_exit = true;
					  boolean sandboxed = Boolean.parseBoolean(bd_doc.getElementsByTagName("IsSandboxed").item(0).getTextContent());
					  String bundleName = KernelUtil.retrieveBundleName(bd_doc);
					  if(sandboxed){
						  System.out.println("This bundle is untrusted, are you sure you wish to continue invoking it (yes/no)?");
						  String reply = Display.scanner.nextLine();
						  while(!reply.equalsIgnoreCase("yes") && !reply.equalsIgnoreCase("no")
									&& !reply.equalsIgnoreCase("y") && !reply.equalsIgnoreCase("n")){
								System.out.println("Please answer yes or no");
								reply = Display.scanner.nextLine();
							}
						  if(reply.equalsIgnoreCase("no") || reply.equalsIgnoreCase("n"))
							  return;
					  }
					  System.out.println("Input Contract Name:");		
					  String cn = Display.scanner.nextLine();
					  if(cn !=null && cn.trim().length()>0){
						  contractName = cn.trim();
						  ArrayList<String> cnames = KernelUtil.retrieveContractNames(bd_doc);
						  boolean cname_exist = false;
						  for(String cname: cnames){
							  if(cname.equals(contractName)){
								  String[] required = KernelUtil.retriveRequiredContracts(bd_doc, contractName);
								  if(required != null){
									  boolean hasAllRequiredContracts = true;
									  for(String contract : required){
										  if(!RemoteLookupService.getContractToBundleMap().containsKey(contract)){
											  System.out.println("Contract requires "+ contract + " contract, which is not available.");
											  hasAllRequiredContracts = false;
										  }
									  }
									  if(!hasAllRequiredContracts){
										  System.out.println("Perhaps run the rlookup command");
										  return;
									  }
								  }
								  cname_exist = true;
								  System.err.println("NOTE: This version of mCom only takes String or primitive parameter types");
								  System.out.println("Please input parameter values as well as it's sensitivity (private or public), separated by the \"|\" key.");
								  System.out.println("Enter \"pr\" to denote private values and \"pu\" (or no sensitivity value) to denote public values: e.g.: Glasgow | pr ");
								 
								  //BUG FIX:mCom does not handle polymorphism (more than one contract with same contractName but different parameters) 
								  HashMap<String, Parameter> evparameters = KernelUtil.retrieveParameters(bd_doc, contractName);
								 
								  Iterator evit = evparameters.entrySet().iterator();
								  while (evit.hasNext()) {
									  Map.Entry evpairs = (Map.Entry)evit.next();
								      String p_name = (String)evpairs.getKey();
								      Parameter pDetails = (Parameter)evpairs.getValue();
								      
								      System.out.println("Input parameter: " + p_name + " value of type:"+ pDetails.getClassName());
								      String p1 = Display.scanner.nextLine();
										
										if(p1 !=null && p1.trim().length()>0){
											p1 = p1.trim();
											
											String[] p1S = p1.split("\\|");
											String paramWithSensitivity = p1;
											p1 = p1S[0].trim();
												
											if(pDetails.getClassName().contains("String")){
												pDetails.setValue(paramWithSensitivity);
												parameters.put(p_name, pDetails);
											}
											else if(pDetails.getClassName().contains("Float") || p_name.contains("float")){
												if(Display.isNumeric(p1)){
													pDetails.setValue(paramWithSensitivity);
													parameters.put(p_name, pDetails);
												}
												else{
													System.err.println("invalid parameter type");
													return;
												}
											}
											else if(pDetails.getClassName().contains("Integer") || p_name.contains("int")){
												if(Display.isNumeric(p1)){
													pDetails.setValue(paramWithSensitivity);
													parameters.put(p_name, pDetails);													
												}
												else{
													System.err.println("invalid parameter type");
													return;
												}
											}
											else if(pDetails.getClassName().contains("Double") || p_name.contains("double")){
												if(Display.isNumeric(p1)){
													pDetails.setValue(paramWithSensitivity);
													parameters.put(p_name, pDetails);	
												}
												else{
													System.err.println("invalid parameter type");
													return;
												}
											}
										}
										else{
											System.err.println("invalid parameter type");
											return;
										}		
								  }
								 String hostAddress =  bd_doc.getElementsByTagName("HostAddress").item(0).getTextContent();
								 String hostPort = bd_doc.getElementsByTagName("HostPort").item(0).getTextContent();
								  if(Initialiser.local_address.getHostAddress().equalsIgnoreCase(hostAddress) && String.valueOf(ReceiverImpl.listenSocket.getLocalPort()).equalsIgnoreCase(hostPort)){
									  Long invocationId = Initialiser.getContractInvocationID();
									  String[] privateParams = KernelUtil.retrievePrivateParams(parameters);
									  Initialiser.invocationPrivateParams.put(invocationId, privateParams);
									  //Initialiser.invocationOrigin.put(invocationId, In)
									  try {
										//long startTime = System.currentTimeMillis();
										InvocationResult result = RemoteMComInvocation.executeLocalCall(invocationId, bundleId, contractName, parameters, null, null);
										//long elapsedTime = System.currentTimeMillis() - startTime;
										//System.out.println(elapsedTime);
										
										if(result != null)
											System.out.println("Response from " + bundleId + "("+ contractName + "): " + result.getResult());
										ContractInvocationLog cIL = SandboxLogger.logContractInvocation(bundleId, bundleName, parameters, invocationId ,Initialiser.local_address.getHostAddress()+ "__" + ReceiverImpl.notifyListenSocket.getLocalPort());
										SandboxLogMonitor.contractInvocationMonitor(cIL, null, null);
										break;
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								  }else{
									 // long startTime = System.currentTimeMillis();
									  sendRemoteCall(bundleId, contractName,parameters,header, null);
									 // long elapsedTime = System.currentTimeMillis() - startTime;
									//	System.out.println(elapsedTime);
									  break;
								  }

							  }
						  }
						  if(!cname_exist){
							  System.err.println("invalid contract name");
							  return;
						  }
					  }
				  }
				}
				if(!bid_exit){
					System.err.println("invalid bundleId");
					return;
				}
			}
		}
		else{
			System.err.println("BundleID is invalid");
			return;
		}		
	}
	
	
	private static void sendRemoteCall(int bundleId, String contractName,	HashMap<String, Parameter> parameters, String header, Policy policy) {
		
		String s [] = header.split("__");
		String bhost_ip = s[0];
		String bhost_port = s[1];
		
		String invoke_request_header = "INVOKEREQUESTHEADER-FROM-"+Initialiser.local_address.getHostAddress()+"__"+ReceiverImpl.listenSocket.getLocalPort()+"-TO-"+bhost_ip+"__"+bhost_port;
		
		String invoke_request_body = "INVOKEREQUESTBODY-";
		Document remoteCallEncoding = KernelUtil.encodeRemoteCallAsxml(bhost_ip, new Integer(bhost_port.trim()),bundleId, contractName, parameters, policy, null);
		invoke_request_body =invoke_request_body+ KernelUtil.getBDString(remoteCallEncoding);
		String invokerMessage = invoke_request_header+invoke_request_body;
		new SenderImpl().sendMessage(bhost_ip, new Integer(bhost_port.trim()), invokerMessage);
	}	
	
	//private static void localCall(int bundleId, String contractName, HashMap<String, String> parameters)
	
	@SuppressWarnings("rawtypes")
	public static String[] interContractInvocation(Long invocationID, String[] invokerDetails, String bundleDescriptorString, String contractName, String[] arguments){
		try{
			Document bdDoc = KernelUtil.decodeTextToXml(bundleDescriptorString);
			Integer bundleId = KernelUtil.retrieveBundleId(bdDoc);
			String hostAddress = bdDoc.getElementsByTagName("HostAddress").item(0).getTextContent();
			String hostPort = bdDoc.getElementsByTagName("HostPort").item(0).getTextContent();
			String bundleName = bdDoc.getElementsByTagName("BundleName").item(0).getTextContent();
			
			//String header = hostAddress + "__" + hostPort;
			String[] privateParams = Initialiser.invocationPrivateParams.get(invocationID);
			if(privateParams != null){
				for(int i = 0; i < privateParams.length; i++){
					for(int j = 0; j < arguments.length; j++){
						if(privateParams[i].contains(arguments[j]) && !arguments[j].trim().endsWith("pr")){
							arguments[j] = arguments[j] + "| pr";
						}
					}
				}
			}
			HashMap<String,Parameter> evparameters = KernelUtil.retrieveParameters(bdDoc, contractName);
			
			Iterator evit = evparameters.entrySet().iterator();
			
			int i = 0;
			while (evit.hasNext()) {
				  Map.Entry evpairs = (Map.Entry)evit.next();
			      String p_name = (String)evpairs.getKey();
			      Parameter pDetails = (Parameter)evpairs.getValue();
			      pDetails.setValue(arguments[i++]);
			      evparameters.put(p_name, pDetails);
			}
			ContractInfo invoker = new ContractInfo(invokerDetails[0], invokerDetails[1], Integer.parseInt(invokerDetails[2]), invokerDetails[3]);
			ContractInfo invokee = new ContractInfo(hostAddress + "__" + hostPort, contractName, bundleId, bundleName);
			
			
			InterContractInvocationLog log ;
			
			InvocationResult result = null;
			String origin = Initialiser.invocationOrigin.get(invocationID);
			if(origin == null){
				origin = invokerDetails[4];
				Initialiser.invocationOrigin.put(invocationID, origin);
			}
			Policy policy = Initialiser.invocationPolicy.get(invocationID) ;
			
			if(policy == null){
				policy = new Policy();
				Document doc = KernelUtil.loadPolicy(invokerDetails[3]);
				policy.setNotice(Boolean.parseBoolean(doc.getElementsByTagName("Notice").item(0).getTextContent()));
				policy.setConsent(Boolean.parseBoolean(doc.getElementsByTagName("Consent").item(0).getTextContent()));
				Initialiser.invocationPolicy.put(invocationID, policy);
			}
			if(Initialiser.local_address.getHostAddress().equalsIgnoreCase(hostAddress) && String.valueOf(ReceiverImpl.listenSocket.getLocalPort()).equalsIgnoreCase(hostPort)){
				
				result = RemoteMComInvocation.executeLocalCall(invocationID, bundleId, contractName, evparameters, policy, origin);
				//InvocationResult invocationResult = new InvocationResult(result, SensitivityLevels.SEMI_PRIVATE);
				
			}else{
				
				String invoke_request_header = "INVOKEREQUESTHEADER-FROM-"+Initialiser.local_address.getHostAddress()+"__"+ReceiverImpl.listenSocket.getLocalPort()+"-TO-"+hostAddress+"__"+hostPort;
			
				String invoke_request_body = "INVOKEREQUESTBODY-";
				Document remoteCallEncoding = KernelUtil.encodeRemoteCallAsxml(hostAddress, new Integer(hostPort.trim()),bundleId, contractName, evparameters, policy, origin);
				invoke_request_body =invoke_request_body+ KernelUtil.getBDString(remoteCallEncoding);
				String invokerMessage = invoke_request_header+invoke_request_body;
			
				result = new SenderImpl().sendContractMessage(hostAddress, new Integer(hostPort.trim()), invokerMessage );
				
			}
			log = new InterContractInvocationLog(invocationID, invoker, invokee, arguments, result);
			log.setOrigin(origin);
			log.setOriginPolicy(policy);
			//SandboxLogger.writeToLogFile(log, KernelConstants.INTER_CONTRACT_INVOCATION_LOG_DIR);
			
			
			KernelUtil.addToInterC(invocationID, log);
			KernelUtil.addToBundleResults(Integer.parseInt(invokerDetails[2]), result);
			return new String[]{result.getResult(), result.getSensitivityLevel().name()};
		
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		
	}
	
}
