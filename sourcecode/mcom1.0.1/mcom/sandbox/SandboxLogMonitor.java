package mcom.sandbox;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import mcom.bundle.Policy;
import mcom.bundle.SensitivityLevels;
import mcom.console.Display;
import mcom.init.Initialiser;
import mcom.kernel.processor.BundleDescriptor;
import mcom.kernel.util.KernelConstants;
import mcom.kernel.util.KernelUtil;
import mcom.sandbox.logs.ContractInfo;
import mcom.sandbox.logs.ContractInvocationLog;
import mcom.sandbox.logs.ContractNotificationLog;
import mcom.sandbox.logs.InterContractInvocationLog;
import mcom.sandbox.logs.InvocationResult;


public class SandboxLogMonitor {
	
	ArrayList<Long> observedInterc = new ArrayList<Long>();
	public void monitor() { 		
		SandboxLogMonitor slm = new SandboxLogMonitor();
		
		Thread invocation_monitor_thread = new Thread(slm.new InvocationMonitor());
		Thread satisfaction_monitor_thread = new Thread(slm.new PolicySatisfactionMonitor());
		satisfaction_monitor_thread.start();
		invocation_monitor_thread.start();	
		
	}
	public static void contractInvocationMonitor(ContractInvocationLog log, Policy policy, String origin){
		
	 
	        boolean satisfiedPolicy = false;
	        boolean partialSatisfaction = false;
	    	//Map.Entry pairs = (Map.Entry)it.next();
	        Long invocationId = log.getInvocationId();
	         if(log.getPrivateArguments().length == 0){
	        	
	       
	        	return;
	        }
	        
	        if(!Initialiser.interContractInvocationLogs.containsKey(invocationId)){
	      
	        	return;
	        }
	        	
	        List<InterContractInvocationLog> interContractLogs = Initialiser.interContractInvocationLogs.get(invocationId);
	        String bundleName = log.getBundleName();
	       
	        boolean consent; 
			boolean notice;
			if(policy != null){
				consent = policy.isConsent();
				notice = policy.isNotice();
			}else{
				Document doc = KernelUtil.loadPolicy(bundleName); 
				consent = Boolean.parseBoolean(doc.getElementsByTagName("Consent").item(0).getTextContent());
				notice = Boolean.parseBoolean(doc.getElementsByTagName("Notice").item(0).getTextContent());
			}
			 
			boolean privateParametersUsed = false;
			String[] contractInvocationPrivateArgs = log.getPrivateArguments();
			for(InterContractInvocationLog interContractLog: interContractLogs){
				if(!log.getBundleName().equalsIgnoreCase(interContractLog.getInvokerContract().getContractBundleName()))
					continue;
			String[] interContractLogArgs = interContractLog.getArguments();
			if(contractInvocationPrivateArgs != null && contractInvocationPrivateArgs.length != 0
					&& interContractLogArgs != null && interContractLogArgs.length != 0){
				for(String arg: contractInvocationPrivateArgs){
					
					for(String interContractLogArg: interContractLogArgs){
						if(interContractLogArg.contains(arg))
					
							privateParametersUsed = true;
					}
				}
			}
			if(privateParametersUsed){
				String rightfulRecipient;
				if(origin != null)
					rightfulRecipient = origin;
				else
					rightfulRecipient = log.getRequesterIpPort();
				if(!consent && !notice)
					return;
				List<ContractNotificationLog> cLogList = Initialiser.consentLogs.get(invocationId);
				List<ContractNotificationLog> nLogList = Initialiser.noticeLogs.get(invocationId);
				ContractNotificationLog cLog = null;
				ContractNotificationLog nLog = null;
				if(cLogList!= null && !cLogList.isEmpty()){
					for(ContractNotificationLog single: cLogList){
						
						if(single.getNotifiee().getIp_port().equalsIgnoreCase(rightfulRecipient))
							cLog = single;
					}
				}
				if(nLogList != null && !nLogList.isEmpty()){
					for(ContractNotificationLog singlee: nLogList){
						
						if(singlee.getNotifiee().getIp_port().equalsIgnoreCase(rightfulRecipient))
							nLog = singlee;
					}
				}
				if(consent && notice){
					
					if(cLog != null && nLog != null && cLog.isGranted())
						satisfiedPolicy = true;
					else if(cLog != null && cLog.isGranted())
						partialSatisfaction = true;
				}else if(consent){
					if(cLog != null && cLog.isGranted())
						satisfiedPolicy = true;
				}else if(notice){
					if(nLog != null)
						satisfiedPolicy = true;
				}
				
				
				if(satisfiedPolicy){
					System.out.println(bundleName + " satisfied policy in contract invocation monitor");
					Initialiser.incrementBundleSatisfiedPolicyCount(bundleName);
					
				}else if(partialSatisfaction){
					System.out.println(bundleName + " partial satisfied policy");
					Initialiser.partialIncrementBundleSatisfiedPolicyCount(bundleName);
				}
				else if(!satisfiedPolicy){
					System.out.println(bundleName + " did not satisfied policy");
					Initialiser.decrementBundleSatisfiedPolicyCount(bundleName);
					
				}
					
			}
			
			else{
				System.out.println("no private parameters used");
				//observed.add(invocationId);
	        	continue;
			}
			}
			  
	}
	class InvocationMonitor implements Runnable{ 
		@Override
		public void run() {
			// TODO Auto-generated method stub
			//long index = 0;
			//ArrayList<Long> observed = new ArrayList<Long>();
			
			while(true){
				//contractInvocationMonitor();
				
				interContractInvocationMonitor();
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				continue;
		}
		}
		
		
		
		private void interContractInvocationMonitor(){
			
			
			
			Iterator it = Initialiser.interContractInvocationLogs.entrySet().iterator();
			
		    while (it.hasNext()) {
		        boolean satisfiedPolicy = false;
		        boolean partialSatisfaction = false;
		    	Map.Entry pairs = (Map.Entry)it.next();
		        Long invocationId = (Long) pairs.getKey();
		      
		        if(observedInterc.contains(invocationId))
		        	continue;
		        List<InterContractInvocationLog> logs = (List<InterContractInvocationLog>)pairs.getValue();
		        for(InterContractInvocationLog log: logs){
		        if(log.getInvocationResult().getSensitivityLevel() != SensitivityLevels.PRIVATE){
		        	observedInterc.add(invocationId);
		        	continue;
		        }
		        String bundleName = log.getInvokerContract().getContractBundleName();
		        Integer bundleId = log.getInvokerContract().getContractBundleId();
		        InvocationResult interContractInvocationResult = log.getInvocationResult();
		        
				boolean sensitiveResultShared = false;
				Document doc = KernelUtil.loadPolicy(bundleName); 
				boolean consent = Boolean.parseBoolean(doc.getElementsByTagName("Consent").item(0).getTextContent());
				boolean notice = Boolean.parseBoolean(doc.getElementsByTagName("Notice").item(0).getTextContent());
				List<InvocationResult> bundleRes = Initialiser.bundleResults.get(bundleId);
					if(bundleRes != null && !bundleRes.isEmpty()){
						for(InvocationResult inv: bundleRes){
							if(inv.equals(interContractInvocationResult)){
								sensitiveResultShared = true;
								break;
							}
						}
					
					
					}
					if(sensitiveResultShared){
						
					
					if(!consent && !notice){
						//observedInterc.add(invocationId);
						continue;
					}
					String interCIpPort = log.getInvokeeContract().getIp_port();
					List<ContractNotificationLog> cLogList = Initialiser.consentLogs.get(invocationId);
					List<ContractNotificationLog> nLogList = Initialiser.noticeLogs.get(invocationId);
					ContractNotificationLog cLog = null;
					ContractNotificationLog nLog = null;
					if(cLogList!= null && !cLogList.isEmpty()){
						for(ContractNotificationLog single: cLogList){
							if(single.getNotifiee().getIp_port().equalsIgnoreCase(interCIpPort));
								cLog = single;
						}
					}
					if(nLogList != null && !nLogList.isEmpty()){
						for(ContractNotificationLog singlee: nLogList){
							if(singlee.getNotifiee().getIp_port().equalsIgnoreCase(interCIpPort))
								nLog = singlee;
						}
					}
					boolean consentSatisfied = cLog != null && cLog.isGranted();
					boolean noticeSatisfied = nLog != null ;
					
					if(consent && notice){
						
						if(consentSatisfied && noticeSatisfied)
							satisfiedPolicy = true;
						else if(consentSatisfied)
							partialSatisfaction = true;
			
					}else if(consent){
						satisfiedPolicy = consentSatisfied;
					} else if(notice)
						satisfiedPolicy = noticeSatisfied;
					
					if(satisfiedPolicy){
						System.out.println(bundleName + " satisfied policy in intercontract invocation monitor");
						Initialiser.incrementBundleSatisfiedPolicyCount(bundleName);
					}
					else if(partialSatisfaction)
						Initialiser.partialIncrementBundleSatisfiedPolicyCount(bundleName);
					else if(!satisfiedPolicy)
						Initialiser.decrementBundleSatisfiedPolicyCount(bundleName);
					
				
				}else{
					//observedInterc.add(invocationId);
					//continue;
				}
					checkInvokedContract(log.getInvokeeContract(), log.getArguments(), logs);
		        }
				observedInterc.add(invocationId);
		    }
		}
		private void checkInvokedContract(ContractInfo invoked, String[] arguments, List<InterContractInvocationLog> logs){
			InterContractInvocationLog nextInChain = null;
			for(InterContractInvocationLog log: logs){
				//System.out.println(log.ge)
				if(log.getInvokerContract().getContractBundleId() == invoked.getContractBundleId())
					nextInChain = log;
			}
			
			if(nextInChain != null){
				 boolean satisfiedPolicy = false;
			        boolean partialSatisfaction = false;
				boolean privateParametersUsed = false;
				for(String arg: arguments){
					
					for(String interContractLogArg: nextInChain.getArguments()){
						if(interContractLogArg.contains(arg))
					
							privateParametersUsed = true;
					}
				}
				if(privateParametersUsed){
					Policy policy = nextInChain.getOriginPolicy();
					String origin = nextInChain.getOrigin();
					boolean consent = policy.isConsent();
					boolean notice = policy.isNotice();
					String bundleName = invoked.getContractBundleName(); 
					Long invocationId = nextInChain.getInvocationID();
					if(!consent && !notice)
						return;
					List<ContractNotificationLog> cLogList = Initialiser.consentLogs.get(invocationId);
					List<ContractNotificationLog> nLogList = Initialiser.noticeLogs.get(invocationId);
					ContractNotificationLog cLog = null;
					ContractNotificationLog nLog = null;
					if(cLogList!= null && !cLogList.isEmpty()){
						for(ContractNotificationLog single: cLogList){
							
							if(single.getNotifiee().getIp_port().equalsIgnoreCase(origin))
								cLog = single;
						}
					}
					if(nLogList != null && !nLogList.isEmpty()){
						for(ContractNotificationLog singlee: nLogList){
							
							if(singlee.getNotifiee().getIp_port().equalsIgnoreCase(origin))
								nLog = singlee;
						}
					}
					if(consent && notice){
						
						if(cLog != null && nLog != null && cLog.isGranted())
							satisfiedPolicy = true;
						else if(cLog != null && cLog.isGranted())
							partialSatisfaction = true;
					}else if(consent){
						if(cLog != null && cLog.isGranted())
							satisfiedPolicy = true;
					}else if(notice){
						if(nLog != null)
							satisfiedPolicy = true;
					}
					
					
					if(satisfiedPolicy){
						//System.out.println(bundleName + " satisfied policy in checkedInvokedContract");
						Initialiser.incrementBundleSatisfiedPolicyCount(bundleName);
						
					}else if(partialSatisfaction){
						//System.out.println(bundleName + " partial satisfied policy");
						Initialiser.partialIncrementBundleSatisfiedPolicyCount(bundleName);
					}
					else if(!satisfiedPolicy){
						//System.out.println(bundleName + " did not satisfied policy");
						Initialiser.decrementBundleSatisfiedPolicyCount(bundleName);
						
					}
				}
			}
		}
	}
	class PolicySatisfactionMonitor implements Runnable{
		List<String> upgraded = new ArrayList<String>();
		List<String> reported = new ArrayList<String>();
		@Override
		public void run() {
				// TODO Auto-generated method stub
			while(true){
				
					for(BundleDescriptor bd: Initialiser.bundleDescriptors){
						//System.out.println("Running policy satisfaction monitor thread");
						if(upgraded.contains(bd.getBundleName())){
							continue;
						}else{
							Float count = Initialiser.getFromBundleSatisfiedPolicyCount(bd.getBundleName());
							//System.out.println(bd.getBundleName() + "'s count: " + count);
							if(count >= KernelConstants.UPGRADE_THRESHOLD){
								Document doc = KernelUtil.loadPolicy(bd.getBundleName());
								boolean consent = Boolean.parseBoolean(doc.getElementsByTagName("Consent").item(0).getTextContent());
								//boolean notice = Boolean.parseBoolean(doc.getElementsByTagName("Notice").item(0).getTextContent());
								if(consent){
									KernelUtil.setConsentPolicy(doc, bd.getBundleName(), false);
								
									upgraded.add(bd.getBundleName());
								}
							//	}
								//upgraded.add(bd.getBundleName());
								System.out.println("Upgrading " + bd.getBundleName() + "'s policy");
							}else if (count <= KernelConstants.VIOLATION_THRESHOLD){
								//KernelUtil.undeploy(bd.getBundleId());
								if(reported.contains(bd.getBundleName()))
									continue;
								System.out.println("Bundle " + bd.getBundleName() + " has reached the threshold for policy violations (3) . Do you choose to undeploy? (yes/no)");
								Display.receivingUndeployInput = true;
								Display.bundleToUndeploy = bd.getBundleId();
								reported.add(bd.getBundleName());
							}
						}	
						
					}
					try {
						Thread.sleep(60000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					continue;
			}
			
		}
		
		
	 }

	

}
