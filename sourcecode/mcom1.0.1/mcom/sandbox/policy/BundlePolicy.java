package mcom.sandbox.policy;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import mcom.bundle.Policy;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class BundlePolicy {
	
	private Policy privateData;
	
	private Policy publicData;
	
	public Policy getPrivateData() {
		return privateData;
	}

	public void setPrivateData(Policy privateData) {
		this.privateData = privateData;
	}

	public Policy getPublicData() {
		return publicData;
	}

	public void setPublicData(Policy publicData) {
		this.publicData = publicData;
	}

	public Document generatePolicyXml(){
		try{
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = null;
		// root elements
			
			doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("BundlePolicy");
			doc.appendChild(rootElement);
			
			Element privateData = doc.createElement("Private");
			//Element semiPrivate = doc.createElement("SemiPrivate");
			Element publicData = doc.createElement("Public");
			Element privateConsent = doc.createElement("Consent");
			Element privateNotice = doc.createElement("Notice");
			Element publicConsent = doc.createElement("Consent");
			Element publicNotice = doc.createElement("Notice");
			publicConsent.appendChild(doc.createTextNode(""+this.publicData.isConsent()));
			
			publicNotice.appendChild(doc.createTextNode(""+this.publicData.isNotice()));
			privateConsent.appendChild(doc.createTextNode(""+this.privateData.isConsent()));
			
			privateNotice.appendChild(doc.createTextNode(""+this.privateData.isNotice()));
			privateData.appendChild(privateConsent);
			privateData.appendChild(privateNotice);
			publicData.appendChild(publicConsent);
			publicData.appendChild(publicNotice);
			rootElement.appendChild(privateData);
			
			rootElement.appendChild(publicData);
		
			return doc;
		}catch(Exception ex){
			return null;
		}
	}
	
	
}
