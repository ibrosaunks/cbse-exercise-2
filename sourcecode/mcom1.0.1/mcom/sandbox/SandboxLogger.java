package mcom.sandbox;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import mcom.init.Initialiser;
import mcom.kernel.impl.Parameter;
import mcom.kernel.util.KernelConstants;
import mcom.kernel.util.KernelUtil;
import mcom.sandbox.logs.ContractInfo;
import mcom.sandbox.logs.ContractInvocationLog;
import mcom.sandbox.logs.ContractNotificationLog;
import mcom.sandbox.logs.InterContractInvocationLog;
import mcom.sandbox.logs.InvocationResult;

public class SandboxLogger {
	
	
	public static void createLogFiles(){
		createFile(KernelConstants.INTER_CONTRACT_INVOCATION_LOG_DIR);
		createFile(KernelConstants.CONTRACT_NOTIFICATION_LOG_DIR);
		
	}
	
	
	public static void writeToLogFile(Object log, String logFileName){
		try(OutputStream file = new FileOutputStream(logFileName, true);
			      OutputStream buffer = new BufferedOutputStream(file);
			      ObjectOutput output = new ObjectOutputStream(buffer);){
			output.writeObject(log);
		}catch(Exception ex){
			
		}
	}
	
	public static void writeNotificationLog(Long invocationID, String[] notifier, String[] notifiee, String notifierType, Boolean granted){
		
		ContractNotificationLog.NotificationType nType = 
				notifierType.equalsIgnoreCase("notice") ? ContractNotificationLog.NotificationType.NOTICE: ContractNotificationLog.NotificationType.CONSENT;
		
		ContractNotificationLog log = new ContractNotificationLog(invocationID, new ContractInfo(notifier[0], notifier[1], Integer.parseInt(notifier[2]), notifier[3]),
				new ContractInfo(notifiee[0], notifiee[1], Integer.parseInt(notifiee[2]), notifiee[3]), nType, granted);
		//writeToLogFile(log, KernelConstants.CONTRACT_NOTIFICATION_LOG_DIR);
		
		if(nType == ContractNotificationLog.NotificationType.CONSENT){
			List<ContractNotificationLog> cnl;
			if(Initialiser.consentLogs.containsKey(log.getInvocationID()))
				cnl = Initialiser.consentLogs.get(log.getInvocationID());
			else
				cnl = Collections.synchronizedList(new ArrayList<ContractNotificationLog>());
			cnl.add(log);
				
			Initialiser.consentLogs.put(log.getInvocationID(), cnl);
		}
		else{
			List<ContractNotificationLog> cnl;
			if(Initialiser.noticeLogs.containsKey(log.getInvocationID()))
				cnl = Initialiser.noticeLogs.get(log.getInvocationID());
			else
				cnl = Collections.synchronizedList(new ArrayList<ContractNotificationLog>());
			cnl.add(log);
				
			Initialiser.noticeLogs.put(log.getInvocationID(), cnl);
		}
	}
	
	/*public static void writeInterContractInvocationLog(long invocationID, ContractInfo invokerContract,
			ContractInfo invokeeContract, Date timeStarted, Date timeCompleted, InvocationResult result){
		
		InterContractInvocationLog log = new InterContractInvocationLog(invocationID, invokerContract,
				invokeeContract, result);
		writeToLogFile(log, KernelConstants.INTER_CONTRACT_INVOCATION_LOG_DIR);
	}*/
	
	
	private static void createFile(String fileName){
		File bundleDir = new File(fileName);
		try{
		// if the directory does not exist, create it
		if (!bundleDir.exists()) {
			// System.out.println("creating Bundle Descriptor Directory: " +
			// bundleDir);
			boolean result = false;
			try {
				bundleDir.createNewFile();
				result = true;
			} catch (SecurityException se) {
				// handle it
			}
			if (!result) {
				System.err.println("error creating " + fileName);
			}
		}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	public static ContractInvocationLog logContractInvocation(Integer bundleId, String bundleName, 	HashMap<String, Parameter> parameters,  Long invocationID, String requesterIpPort) {
		// TODO Auto-generated method stub
		
		
	    ContractInvocationLog cIL = new ContractInvocationLog(invocationID, KernelUtil.retrievePrivateParams(parameters), bundleName, bundleId);
	    cIL.setRequesterIpPort(requesterIpPort);
	    Initialiser.contractInvocationLogs.put(invocationID, cIL);
	    return cIL;
	}
	
	public static ContractInvocationLog logContractInvocation(Integer bundleId, String bundleName, 	Object[] arguments,  Long invocationID, String requesterIpPort){
		
	    
	    ContractInvocationLog cIL = new ContractInvocationLog(invocationID, KernelUtil.retrievePrivateParams(arguments), bundleName, bundleId);
	    cIL.setRequesterIpPort(requesterIpPort);
	    Initialiser.contractInvocationLogs.put(invocationID, cIL);
	    return cIL;
	    //andboxLogMonitor.contractInvocationMonitor(cIL);
	}
}
