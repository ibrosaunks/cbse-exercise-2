package mcom.sandbox.logs;

import java.io.Serializable;

public class ContractInfo  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String ip_port;
	
	String contractName;
	
	int contractBundleId;
	
	String contractBundleName;
	
	public ContractInfo(){}
	
	public ContractInfo(String ip_port, String contractName,int contractBundleId, String contractBundleName){
		setIp_port(ip_port);
		setContractName(contractName);
		setContractBundleId(contractBundleId);
		setContractBundleName(contractBundleName);
	}
	public String getIp_port() {
		return ip_port;
	}

	public void setIp_port(String ip_port) {
		this.ip_port = ip_port;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public int getContractBundleId() {
		return contractBundleId;
	}

	public void setContractBundleId(int contractBundleId) {
		this.contractBundleId = contractBundleId;
	}

	public String getContractBundleName() {
		return contractBundleName;
	}

	public void setContractBundleName(String contractBundleName) {
		this.contractBundleName = contractBundleName;
	}
	
	public String toString(){
		return contractName + " in bundle " + contractBundleName;
		
	}
}
