package mcom.sandbox.logs;

public class ContractInvocationLog {
	
	private Long invocationId;
	private String[] privateArguments;
	private String bundleName;
	private Integer bundleId;
	private String requesterIpPort;
	
	public ContractInvocationLog(Long invocationId, String[] argu, String bundleName, Integer bundleId){
		this.invocationId = invocationId;
		this.privateArguments = argu;
		this.bundleName = bundleName;
		this.bundleId = bundleId;
		
	}
	public Long getInvocationId() {
		return invocationId;
	}
	public void setInvocationId(Long invocationId) {
		this.invocationId = invocationId;
	}
	//private InvocationResult result;
	public String[] getPrivateArguments() {
		return privateArguments;
	}

	public void setPrivateArguments(String[] arguments) {
		this.privateArguments = arguments;
	}
	public String getBundleName() {
		return bundleName;
	}
	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}
	public Integer getBundleId() {
		return bundleId;
	}
	public void setBundleId(Integer bundleId) {
		this.bundleId = bundleId;
	}
	public String getRequesterIpPort() {
		return requesterIpPort;
	}
	public void setRequesterIpPort(String requesterIpPort) {
		this.requesterIpPort = requesterIpPort;
	}

	
	/*public InvocationResult getResult() {
		return result;
	}

	public void setResult(InvocationResult result) {
		this.result = result;
	}*/

	
	
	
}
