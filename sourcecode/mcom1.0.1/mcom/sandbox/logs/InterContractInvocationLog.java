package mcom.sandbox.logs;

import java.io.Serializable;
import java.util.Date;

import mcom.bundle.Policy;

public class InterContractInvocationLog implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long invocationID;
	
	private ContractInfo invokerContract;
	
	private ContractInfo invokeeContract;
	
	private String[] arguments;
	
	private InvocationResult invocationResult;
	
	
	private String origin;
	
	private Policy originPolicy;
	/*private Date timeStarted;
	
	private Date timeCompleted;*/
	
	public InterContractInvocationLog(){}
	
	public InterContractInvocationLog(long invocationID, ContractInfo invokerContract, ContractInfo invokeeContract, String[] arg,  InvocationResult result){
		setInvocationID(invocationID);
		setInvokerContract(invokerContract);
		setInvokeeContract(invokeeContract);
		setArguments(arg);
		//setTimeStarted(timeStarted);
		setInvocationResult(result);
	}
	/*public InterContractInvocationLog(long invocationID, ContractInfo invokerContract, ContractInfo invokeeContract,String[] arg, Date timeStarted, Date timeCompleted, InvocationResult result){
		this(invocationID, invokerContract, invokeeContract, timeStarted, result);
		setTimeCompleted(timeCompleted);
	}*/
	public long getInvocationID() {
		return invocationID;
	}

	public void setInvocationID(long invocationID) {
		this.invocationID = invocationID;
	}

	public ContractInfo getInvokerContract() {
		return invokerContract;
	}

	public void setInvokerContract(ContractInfo invokerContract) {
		this.invokerContract = invokerContract;
	}

	public ContractInfo getInvokeeContract() {
		return invokeeContract;
	}

	public void setInvokeeContract(ContractInfo invokeeContract) {
		this.invokeeContract = invokeeContract;
	}

	/*public Date getTimeStarted() {
		return timeStarted;
	}

	public void setTimeStarted(Date timeStarted) {
		this.timeStarted = timeStarted;
	}

	public Date getTimeCompleted() {
		return timeCompleted;
	}

	public void setTimeCompleted(Date timeCompleted) {
		this.timeCompleted = timeCompleted;
	}
*/
	public InvocationResult getInvocationResult() {
		return invocationResult;
	}

	public void setInvocationResult(InvocationResult invocationResult) {
		this.invocationResult = invocationResult;
	}

	public String[] getArguments() {
		return arguments;
	}

	public void setArguments(String[] arguments) {
		this.arguments = arguments;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public Policy getOriginPolicy() {
		return originPolicy;
	}

	public void setOriginPolicy(Policy originPolicy) {
		this.originPolicy = originPolicy;
	}
	
	
}
