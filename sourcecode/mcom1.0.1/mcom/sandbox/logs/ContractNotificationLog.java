package mcom.sandbox.logs;

import java.io.Serializable;
import java.util.Date;

public class ContractNotificationLog implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long invocationID;
	
	//private Date startTime;
	
	private ContractInfo notifier;
	
	private ContractInfo notifiee;
	
	private NotificationType notificationType;
	
	private boolean granted;
	
	public ContractNotificationLog(){}
	
	public ContractNotificationLog(long invocationID, ContractInfo notifier, ContractInfo notifiee, ContractNotificationLog.NotificationType nType, boolean granted){
		setInvocationID(invocationID);
		//setStartTime(startTime);
		setNotifier(notifier);
		setNotifiee(notifiee);
		setNotificationType(nType);
		setGranted(granted);
		
	}
	public long getInvocationID() {
		return invocationID;
	}

	public void setInvocationID(long invocationID) {
		this.invocationID = invocationID;
	}

	/*public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}*/

	public ContractInfo getNotifier() {
		return notifier;
	}

	public void setNotifier(ContractInfo notifier) {
		this.notifier = notifier;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public boolean isGranted() {
		return granted;
	}

	public void setGranted(boolean granted) {
		this.granted = granted;
	}

	
	
	public ContractInfo getNotifiee() {
		return notifiee;
	}

	public void setNotifiee(ContractInfo notifiee) {
		this.notifiee = notifiee;
	}

	

	public enum NotificationType implements Serializable{
		
		CONSENT, NOTICE;
		
	}
}
