package mcom.sandbox.logs;
import java.io.Serializable;

import mcom.bundle.SensitivityLevels;
public class InvocationResult implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String result;
	
	private SensitivityLevels sensitivityLevel;
	
	public InvocationResult(String result2, SensitivityLevels sensitivi) {
		// TODO Auto-generated constructor stub
		setResult(result2);
		setSensitivityLevel(sensitivi);
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public SensitivityLevels getSensitivityLevel() {
		return sensitivityLevel;
	}

	public void setSensitivityLevel(SensitivityLevels sensitivityLevel) {
		this.sensitivityLevel = sensitivityLevel;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((this.result == null) ? 0 : this.result.hashCode());
		result = prime
				* result
				+ ((sensitivityLevel == null) ? 0 : sensitivityLevel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvocationResult other = (InvocationResult) obj;
		if (result == null) {
			if (other.result != null)
				return false;
		} else if (!result.equals(other.result))
			return false;
		if (sensitivityLevel != other.sensitivityLevel)
			return false;
		return true;
	}

}
