/**
 * mComponent Initialiser:
 * (1) initialise local host address
 * (2) start a Receiver to listen to incoming messages
 * (3) creates a display for console interaction
 *
 * @Author Inah Omoronyia School of Computing Science, University of Glasgow
 */

package mcom.init;


import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;





/*import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonMethod;*/
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;






import mcom.bundle.Policy;
import mcom.console.Display;
import mcom.kernel.processor.BundleDescriptor;
import mcom.kernel.util.KernelConstants;
import mcom.sandbox.SandboxLogMonitor;
import mcom.sandbox.SandboxLogger;
import mcom.sandbox.logs.ContractInvocationLog;
import mcom.sandbox.logs.ContractNotificationLog;
import mcom.sandbox.logs.InterContractInvocationLog;
import mcom.sandbox.logs.InvocationResult;
import mcom.wire.Receiver;
import mcom.wire.impl.ReceiverImpl;
import mcom.wire.util.DynamicRegistrarDiscovery;
import mcom.wire.util.IPResolver;
import mcom.wire.util.RegistrarService;


public class Initialiser {

	private static Map<String, Object> objectsMap; // TODO: Change or keep?
	private static Map<String, Object> persistentObjectsMap;
	
	public static Map<String, Object> getPersistentObjectsMap() {
		return persistentObjectsMap;
	}

	public static void setPersistentObjectsMap(
			Map<String, Object> persistentObjectsMap) {
		Initialiser.persistentObjectsMap = persistentObjectsMap;
	}

	private static Map<String, Class> controllerMap;

	public static String receiver_ip;
	public static int receiver_listening_port = -1;

	public static InetAddress local_address = null;
	public static Receiver receiver;
	public static RegistrarService reg_ser;
	public static DynamicRegistrarDiscovery dynDis;

	public static BundleDescriptor[] bundleDescriptors;
	
	//public List<InterContractInvocationLog> mapLog = Collections.synchronizedList(new ArrayList<InterContractInvocationLog>());
	public static Map<Integer, List<InvocationResult>> bundleResults = new HashMap<Integer, List<InvocationResult>>();
	public static Map<Long, List<InterContractInvocationLog>> interContractInvocationLogs = Collections.synchronizedMap(new HashMap<Long, List<InterContractInvocationLog>>()); 
	
	public static Map<Long, ContractInvocationLog>  contractInvocationLogs = Collections.synchronizedMap(new HashMap<Long, ContractInvocationLog>());
	
	public static Map<Long, List<ContractNotificationLog>> consentLogs = Collections.synchronizedMap(new HashMap<Long, List<ContractNotificationLog>>());
	
	public static Map<Long,  List<ContractNotificationLog>> noticeLogs = Collections.synchronizedMap(new HashMap<Long,  List<ContractNotificationLog>>());
	
	public static Map<String, Float> bundleSatisfiedPolicyCount = Collections.synchronizedMap(new HashMap<String, Float>());
	
	public static Map<Long, String[]> invocationPrivateParams = Collections.synchronizedMap(new HashMap<Long, String[]>());
	
	public static Map<Long, String> invocationOrigin = Collections.synchronizedMap(new HashMap<Long, String>());
	
	public static Map<Long, Policy> invocationPolicy = Collections.synchronizedMap(new HashMap<Long, Policy>());
	
	private static SandboxLogMonitor monitor;
	public static final String VERSION = "1.0.1";
	
	private static Long lastContractInvocationID = 0l;
	
	
	/**
	 * Main Method; initialises the Session Maps, Receivers, Creates the Bundle Directory, sets IP Address and Listening ports
	 * 
	 *@param args[]
	 * 
	 * 
	 */
	public static void main(String args[]) {
		try {

			objectsMap = new HashMap<String, Object>();

			controllerMap = new HashMap<String, Class>();

			local_address = IPResolver.getLocalHostLANAddress();
			createBundleDirectory();
			
			
			bundleDescriptors = new BundleDescriptor[0];
			SandboxLogger.createLogFiles();
			
			startReceiver();
			startSandboxMonitor();
			new Display();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	private static void startReceiver() {
		receiver = new ReceiverImpl();
		receiver.receiveMessage();
	}
	
	private static void startSandboxMonitor(){
		monitor = new SandboxLogMonitor();
		monitor.monitor();
	}
	/*
	 * Creates BundleDir - Directory where all mCom bundles are stored
	 */
	private static void createBundleDirectory() {
		createDirectory(KernelConstants.BUNDLEDIR);
		createDirectory(KernelConstants.BUNDLEDESCRIPTORDIR);
		createDirectory(KernelConstants.BUNDLEPOLICYDIR);
		//createDirectory(KernelConstants.LOGDIR);
		// if the directory does not exist, create it
		
	}
	
	private static void createDirectory(String fileName){
		File bundleDir = new File(fileName);

		// if the directory does not exist, create it
		if (!bundleDir.exists()) {
			// System.out.println("creating Bundle Descriptor Directory: " +
			// bundleDir);
			boolean result = false;
			try {
				bundleDir.mkdir();
				result = true;
			} catch (SecurityException se) {
				// handle it
			}
			if (!result) {
				System.err.println("error creating " + fileName);
			}
		}
	}

	/**
	 * Add Bundle Descriptor object to list of Bundle Descriptors 
	 * @param bd Bundle Descriptor object to add
	 */
	public static void addBundleDescriptor(BundleDescriptor bd) {
		BundleDescriptor[] bd_temp = new BundleDescriptor[bundleDescriptors.length + 1];
		List<BundleDescriptor> bd_t = new LinkedList<BundleDescriptor>();

		for (BundleDescriptor bdt : bundleDescriptors) {
			if (bdt != null) {
				bd_t.add(bdt);
			}
		}

		int i = 0;
		for (BundleDescriptor bdt : bd_t) {
			bd_temp[i] = bdt;
			i = i + 1;
		}

		bd_temp[i] = bd;
		bundleDescriptors = bd_temp;
	}
	
	public static void removeBundleDescriptor(BundleDescriptor bd){
		List<BundleDescriptor> list = new ArrayList<BundleDescriptor>(Arrays.asList(bundleDescriptors));
		
		Iterator<BundleDescriptor> iter = list.iterator();
		while(iter.hasNext()){
			BundleDescriptor bundleDesc = iter.next();
			if(bd.getBundleId() == bundleDesc.getBundleId())
				iter.remove();
		}
		
		bundleDescriptors = list.toArray(new BundleDescriptor[list.size()]);
	}
	
	public static boolean objectExists(Class clc) {

		if (objectsMap.containsKey(clc.getName())) {
			return true;
		}

		return false;
	}

	public static void addObject(Class clc, Object obj) {

		objectsMap.put(clc.getName(), obj);

	}

	public static Object getObject(Class clc) {

		return objectsMap.get(clc.getName());

	}

	public static boolean isControllerExists(String clc) {

		if (controllerMap.containsKey(clc)) {
			return true;
		}
		return false;
	}

	public static void addController(String clc, Class obj) {
		controllerMap.put(clc, obj);
	}

	public static Class getController(String clc) {
		return controllerMap.get(clc);
	}
	
	/**
	 * Saves Persistent session in file 
	 * 
	 * 
	 * @param  className The name of the Bundle Entity Class 
	 * @param object The Bundle Entity Object to be serialized and saved to file
	 */

	public static void savePersistentSession(String className, Object object) {

		ObjectMapper mapper = new ObjectMapper();

		mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		try {
			//mapper.setVisibility(JsonMethod.FIELD, Visibility.ANY);
			mapper.writeValue(
					new File("sessions/" + className + "session.json"), object);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// FileUtil.writeSessionToFile(persistentObjectsMap);

	}
	
	public static synchronized long getContractInvocationID(){
		
		return ++lastContractInvocationID;
	}
	
	public static synchronized void putInBundleSatisfiedPolicyCount(String bundleName, Float count ){
		bundleSatisfiedPolicyCount.put(bundleName, count);
	}
	
	public static synchronized Float  getFromBundleSatisfiedPolicyCount(String bundleName){
		if(bundleSatisfiedPolicyCount.containsKey(bundleName)){
			return bundleSatisfiedPolicyCount.get(bundleName);
		}else
			return 0.0f;
	}
	
	public static synchronized void incrementBundleSatisfiedPolicyCount(String bundleName){
		Float count = getFromBundleSatisfiedPolicyCount(bundleName);
		
		count++;
		putInBundleSatisfiedPolicyCount(bundleName, count);
		
		
	}
	
	public static synchronized void partialIncrementBundleSatisfiedPolicyCount(String bundleName){
		Float count = getFromBundleSatisfiedPolicyCount(bundleName);
		
		count = count + 0.5f;
		putInBundleSatisfiedPolicyCount(bundleName, count);
		
		
	}
	
	public static synchronized void decrementBundleSatisfiedPolicyCount(String bundleName) {
		// TODO Auto-generated method stub
		Float count = getFromBundleSatisfiedPolicyCount(bundleName);
		System.out.println("decrementing " + bundleName);
		count--;
		putInBundleSatisfiedPolicyCount(bundleName, count);
		
	}

	/*public static synchronized List<InterContractInvocationLog> getInterContractInvocationLogs() {
		return interContractInvocationLogs;
	}

	public static synchronized void setInterContractInvocationLogs(
			List<InterContractInvocationLog> interContractInvocationLogs) {
		Initialiser.interContractInvocationLogs = interContractInvocationLogs;
	}
	
	public static synchronized void addToInterContractInvocationLogs(InterContractInvocationLog log){
		getInterContractInvocationLogs().add(log);
		
	}*/
}
