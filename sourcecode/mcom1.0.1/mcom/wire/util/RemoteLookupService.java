package mcom.wire.util;
/**
 * @Author Inah Omoronyia School of Computing Science, University of Glasgow 
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import mcom.init.Initialiser;
import mcom.kernel.util.KernelUtil;
import mcom.wire.impl.ReceiverImpl;
import mcom.wire.impl.SenderImpl;

public class RemoteLookupService {
	private static Map<String,String> lookedUpAdverts  = new HashMap<String, String>();;//<String:header, String:body>
	private static Map<String, ArrayList<String>> contractToBundleMap = new HashMap<String, ArrayList<String>>() ;
	

	public static void doRemoteLookup(){
		
		
		//lookup on all known Registers
		String message = "LOOKUPADVERTS-"+Initialiser.local_address.getHostAddress()+"__"+ReceiverImpl.listenSocket.getLocalPort();
		if(DynamicRegistrarDiscovery.getActiveRegistrars() == null || DynamicRegistrarDiscovery.getActiveRegistrars().size() ==0){
			System.err.println("No known Registrar");
		}
		else{
			for(String regip_port:DynamicRegistrarDiscovery.getActiveRegistrars()){
				String [] res = regip_port.split("__");
				String serviceip = res[0];
				String serviceport = res[1];
				new SenderImpl().sendMessage(serviceip, new Integer(serviceport), message);	
			}
		}
	}
	
	public static void addLookupResult(String header, String body){
		lookedUpAdverts.put(header, body);
		addToContractToBundleMap(body);
	}
	
	private static void addToContractToBundleMap(String body){
		Document bd_doc = KernelUtil.decodeTextToXml(body.trim());
		
		 ArrayList<String> cnames = KernelUtil.retrieveContractNames(bd_doc);
		 for(String contract: cnames){
			 //System.out.println(contract);
			 if(contractToBundleMap.containsKey(contract)){
				 if(contractToBundleMap.get(contract).contains(body))
					 continue;
				 else
					 contractToBundleMap.get(contract).add(body);
			 }else{
				 ArrayList<String> contractArrayList = new ArrayList<String>();
				 contractArrayList.add(body);
				 contractToBundleMap.put(contract,contractArrayList);
			 }
		 }
	}
	
	public static ArrayList<String> findBundlesByContract(String contractName){
		
		if(contractToBundleMap.containsKey(contractName)){
			return contractToBundleMap.get(contractName);
		}else{
			return null;
		}
	}
	public static void removeLookupResult(String header){
		lookedUpAdverts.remove(header);
	}
	
	public static Map<String,String> getLookupResults(){
		return lookedUpAdverts;
	}
	
	public static Map<String, ArrayList<String>> getContractToBundleMap() {
		return contractToBundleMap;
	}
}
