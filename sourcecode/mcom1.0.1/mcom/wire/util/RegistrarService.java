package mcom.wire.util;
/**
 * @Author Inah Omoronyia School of Computing Science, University of Glasgow 
 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import mcom.init.Initialiser;
import mcom.wire.impl.ReceiverImpl;
import mcom.wire.impl.SenderImpl;

public class RegistrarService {
	public static boolean isRegistrarService = false;
	private static Map<String,String> registeredAdverts;//<String:header, String:body>
	private static MulticastSocket socket;
	private static Thread t_thread;
	
	/**
	 * Starts registrar service on instance by joining Multicast group and setting registrarService flag to true
	 * 
	 * 
	 *  
	 * 
	 */
	public void startRegistrarService() {
		isRegistrarService = true;
		registeredAdverts = new HashMap<String, String>();
		InetAddress multicastAddressGroup;
		try {
			if(Initialiser.local_address instanceof Inet6Address){
				multicastAddressGroup = InetAddress.getByName(RegistrarConstants.MULTICAST_ADDRESS_GROUP_IPV6);				
			}
			else{
				multicastAddressGroup = InetAddress.getByName(RegistrarConstants.MULTICAST_ADDRESS_GROUP_IPV4);
			}
			int multicastPort = RegistrarConstants.MULTICAST_PORT;
			
			socket = new MulticastSocket(multicastPort);
			socket.joinGroup(multicastAddressGroup);
			
		} 
		catch (UnknownHostException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		t_thread = new Thread(new RegistrarService().new RegistrarServiceRunner());
		t_thread.start();
		
		System.out.println("RegistrarService active: "+isRegistrarService);		
	}
	
	/**
	 * Add an advertisement to map of adverts
	 * @param header advert header, contains address of host
	 * @param body bundle descriptor string
	 */
	public static void addAdvert(String header, String body){
		if(isRegistrarService){
			registeredAdverts.put(header, body);
		}		
	}
	
	/**
	 * Removes an advert from map of adverts
	 * @param header address of host
	 */
	public static void removeAdvert(String header){
		if(registeredAdverts.containsKey(header))
			registeredAdverts.remove(header);
	}
	
	/**
	 * Gets all adverts in a Map
	 * 
	 * @return The map of all adverts
	 */
	public static Map<String,String> getAdverts(){
		return registeredAdverts;
	}
	
	class RegistrarServiceRunner implements Runnable {
		public void run() {
			try {
				while(isRegistrarService){
					byte[] buf = new byte[1000];
					DatagramPacket receivedPacket = new DatagramPacket(buf, buf.length);

					socket.receive(receivedPacket);
					String recStr = new String(receivedPacket.getData()).trim(); //valid format: REGPING

					System.out.println(recStr.trim());

					if(recStr.startsWith("REGPING")){
						//respond to request							
						String [] res0 = recStr.split("REGPING-");
						String [] res = res0[1].split("__");
						String serviceip = res[0].trim();
						String serviceport = res[1].trim();

						String resStr = "REGACCEPT-"+Initialiser.local_address.getHostAddress()+"__"+ReceiverImpl.listenSocket.getLocalPort();							
						new SenderImpl().sendMessage(serviceip, new Integer(serviceport), resStr);														
					}					
				}
			}
			catch (IOException ioe) {}
		}
	}
	
	public void stopRegistrarService() {
		isRegistrarService = false;	
		registeredAdverts = null;
		t_thread.interrupt();
		socket.close();
		System.out.println("RegistrarService active: "+isRegistrarService);
		
	}
}
