/**
 * The Receiver starts a thread on the background that listens to incoming messages
 * and respond to connection request.
 * @Author Inah Omoronyia School of Computing Science, University of Glasgow 
 */

package mcom.wire.impl;

import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.io.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import mcom.bundle.Policy;
import mcom.console.Display;
import mcom.init.Initialiser;

import java.util.List;

import mcom.kernel.impl.RemoteMComInvocation;
import mcom.kernel.util.KernelUtil;
import mcom.sandbox.logs.ContractInvocationLog;
import mcom.sandbox.logs.InvocationResult;
import mcom.wire.Receiver;
import mcom.wire.util.DynamicRegistrarDiscovery;
import mcom.wire.util.IPResolver;
import mcom.wire.util.RegistrarService;
import mcom.wire.util.RemoteLookupService;
import mcom.sandbox.SandboxLogMonitor;
import mcom.sandbox.SandboxLogger;

public class ReceiverImpl implements Receiver{
	public static ServerSocket listenSocket;
	public static ServerSocket notifyListenSocket;
	public void receiveMessage() { 		
		ReceiverImpl rImpl = new ReceiverImpl();
		Thread server_thread = new Thread(rImpl.new ReceiverRunner());
		Thread notify_thread = new Thread(rImpl.new NotifyReceiverRunner());

		server_thread.start();	
		notify_thread.start();
	}
	
	class ReceiverRunner implements Runnable {
			
		@SuppressWarnings("rawtypes")
		public void run() {
			listenSocket = IPResolver.configureHostListeningSocket(); 	
			
			while(true) {
				try {
					Socket clientSocket = listenSocket.accept();	
					if(clientSocket !=null){
						DataInputStream in= new DataInputStream(clientSocket.getInputStream());
						DataOutputStream out =new DataOutputStream(clientSocket.getOutputStream());

						String r_message = in.readUTF();

						if(r_message.equals("ping")){ //client checking recipient existence
							boolean accepted = acceptPing(clientSocket);					
							String response = "";
							if(accepted){
								response = "accepted";	
								send(response, out);
								closeConnection(clientSocket);
							}
							else{
								response = "rejected";	
								send(response,out);
								closeConnection(clientSocket);
							}
						}
						
						else if(r_message.startsWith("REGACCEPT")){
							System.out.println(Display.ansi_normal.colorize("["+clientSocket.getInetAddress()+"__"+clientSocket.getPort()+"]"+r_message));
							
							String [] res = r_message.split("REGACCEPT-");
							String regip_port = res[1];
							DynamicRegistrarDiscovery.addActiveRegistrar(regip_port);
							
							send(""+clientSocket.getInetAddress()+"__"+clientSocket.getPort()+" ack", out);
							closeConnection(clientSocket);
						}
						
						else if(r_message.startsWith("ADVERTHEADER-")){
							
							
							String [] res = r_message.split("ADVERTBODY-");
							String part0 = res[0];
							String body = res[1];
							
							String []res1 = part0.split("ADVERTHEADER-");
							String header = res1[1];
							
							RegistrarService.addAdvert(header, body);
							
							send(""+clientSocket.getInetAddress()+"__"+clientSocket.getPort()+" ack", out);
							closeConnection(clientSocket);
						}else if(r_message.startsWith("UNADVERTHEADER-")){
							
							
							String [] res = r_message.split("UNADVERTBODY-");
							String part0 = res[0];
							String []res1 = part0.split("UNADVERTHEADER-");
							String header = res1[1];
							
							RegistrarService.removeAdvert(header);
							send(""+clientSocket.getInetAddress()+"__"+clientSocket.getPort()+"unadvert ack", out);
							closeConnection(clientSocket);
						}
						else if(r_message.startsWith("LOOKUPADVERTS-")){
							
							String [] res = r_message.split("LOOKUPADVERTS-");
							String regip_port = res[1];	
							String[] add_split = regip_port.split("__"); 
							String ip = add_split[0];
							
							if(RegistrarService.isRegistrarService){								
								Iterator it = RegistrarService.getAdverts().entrySet().iterator();
							    while (it.hasNext()) {
							    	
							        Map.Entry pairs = (Map.Entry)it.next();
							        String header = (String)pairs.getKey();
							        String body = (String)pairs.getValue();	
							        Document in_doc = KernelUtil.decodeTextToXml(body);
							        
							        String host_ip = in_doc.getElementsByTagName("HostAddress").item(0).getTextContent();
							        NodeList contracts = in_doc.getElementsByTagName("Contracts").item(0).getChildNodes();
							        //System.out.println(host_ip);
							        //System.out.println("contract content: )
							        for(int i=0; i < contracts.getLength(); i++){
							        	Node contract = contracts.item(i);
							        	
							        	String accessLevel = contract.getChildNodes().item(5).getTextContent();
							        	
							        	if(!hasAccess(accessLevel, ip , host_ip)){
							        		
							        		contract.getParentNode().removeChild(contract);
							        		in_doc.normalize();
							        	}
							        }
							        
							        body = KernelUtil.getBDString(in_doc);
							        body = KernelUtil.prettyPrint(body);
							        String message = "LOOKUPRESPONSEHEADER-"+header+"LOOKUPRESPONSEBODY-"+body;
									
							        send(message, out);
							    }
							    send("END", out);
							}
							closeConnection(clientSocket);
						}
						else if(r_message.contains("LOOKUPRESPONSEHEADER-")){
							String [] s1 = r_message.split("LOOKUPRESPONSEHEADER-");
							String [] s2 = s1[1].split("LOOKUPRESPONSEBODY-");
							String header = s2[0];
							String body = s2[1];
							
							RemoteLookupService.addLookupResult(header, body);
							System.out.println("Lookup response:");
							System.out.println("ContractHost: "+header.trim());
							System.out.println(body.trim());
						}
						else if(r_message.contains("INVOKEREQUESTHEADER-")){ //invocation server
							
							/*System.out.println("Inside invoke request");
							System.out.println(r_message);*/
							
							String de[] = r_message.split("INVOKEREQUESTHEADER-FROM-");							
							String d1 = de[1];
							String d2 []= d1.split("-TO-");
							String fromipport = d2[0];
							String f[] = fromipport.split("__");
							String fromip = f[0].trim();
							String fromport = f[1].trim();
							String d3 = d2[1];
							String d4[] = d3.split("INVOKEREQUESTBODY-");
							String toipport = d4[0];
							String t[] = toipport.split("__");
							String toip = t[0];
							String toport = t[1];
							String invokebody = d4[1];
														
							Document inv_doc = KernelUtil.decodeTextToXml(invokebody.trim());
							String bundleId = inv_doc.getElementsByTagName("BundleId").item(0).getTextContent();
							String notificationPort = inv_doc.getElementsByTagName("NotificationPort").item(0).getTextContent();
							Long invocationId = Initialiser.getContractInvocationID();
							NodeList parameters = inv_doc.getElementsByTagName("Contract").item(0).getChildNodes().item(1).getChildNodes();
							Object[] arguments  = new Object[parameters.getLength()];
							for(int i=0; i<parameters.getLength();i++){
								
								
								arguments[i] = parameters.item(i).getChildNodes().item(1).getTextContent();
							}
							Policy pol = KernelUtil.extractPolicyFromRemoteCall(inv_doc);
							String origin = null ;
							if(pol != null){
								Initialiser.invocationPolicy.put(invocationId, pol);
								origin = inv_doc.getElementsByTagName("PrivateDataOrigin").item(0).getTextContent();
							}
							//System.out.println(origin);
							if(origin != null){
								Initialiser.invocationOrigin.put(invocationId, origin);
							}
							String[] privateParams = KernelUtil.retrievePrivateParams(arguments);
							Initialiser.invocationPrivateParams.put(invocationId, privateParams);
							InvocationResult result = RemoteMComInvocation.executeRemoteCall(inv_doc, fromipport, invocationId, origin, pol);
							
							
							Element dresult = inv_doc.createElement("Result");
						    dresult.appendChild(inv_doc.createTextNode(""+result.getResult()));
						    inv_doc.getLastChild().appendChild(dresult);
						    
						    Element dsensitivityLevel = inv_doc.createElement("SensitivityLevel");
						    dsensitivityLevel.appendChild(inv_doc.createTextNode(""+result.getSensitivityLevel().name()));
						    inv_doc.getLastChild().appendChild(dsensitivityLevel);
							String response_header = "INVOKERRESPONSEHEADER-FROM-"+toip.trim()+"__"+toport.trim()+"-TO-"+fromip.trim()+"__"+fromport.trim();
							
							String invoke_response_body = "INVOKERESPONSEBODY-";
							invoke_response_body =invoke_response_body+ KernelUtil.getBDString(inv_doc);
							String invokerMessage = response_header+invoke_response_body;
							
							
							
							//TODO: Select a method
							
							//Method 1
							send(invokerMessage, out);
							closeConnection(clientSocket);
							KernelUtil.addToBundleResults(new Integer(bundleId), result);
							ContractInvocationLog cIL = SandboxLogger.logContractInvocation(new Integer(bundleId),KernelUtil.loadBundleDescriptor(bundleId).getBundleName(), arguments, invocationId ,fromip + "__" + notificationPort);
							
							SandboxLogMonitor.contractInvocationMonitor(cIL, pol, origin);
							
							//Method 2
							//new SenderImpl().sendMessage(fromip, new Integer(fromport), invokerMessage);
							
						}						
						else if(r_message.contains("INVOKERRESPONSEHEADER-")){ //invocation client
							String dr [] = r_message.split("INVOKERRESPONSEHEADER-FROM-");
							String d1 = dr[1];
							String d2 [] = d1.split("INVOKERESPONSEBODY-");
							//String fromtoipport = d2[0];
							String responsebody = d2[1];			
							//print response body
							System.out.println(KernelUtil.prettyPrint(responsebody));
							closeConnection(clientSocket);

						}
						
						else{
							System.out.println(Display.ansi_normal.colorize("["+clientSocket.getInetAddress()+"__"+clientSocket.getPort()+"]"+r_message));
							closeConnection(clientSocket);
						}	
					}
				} 
				catch(EOFException e) {
					System.out.println("EOF:"+e.getMessage());
				} 
				catch(IOException e) {
					System.out.println("IO:"+e.getMessage());
				}
				 
			}
		}
		
		private boolean acceptPing(Socket clientSocket){		
			System.out.println(Display.ansi_normal2.colorize("now connected to "+clientSocket.getInetAddress()+"__"+clientSocket.getPort()));
			boolean accepted = true;
			
			return accepted;
		}
		private void send(String message,DataOutputStream out){
			if(out !=null){
				try {
					out.writeUTF(message);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			else{
		    	System.out.println(Display.ansi_error.colorize("ERROR:No receiver"));
			}
		}
		
		
	}
	
	class NotifyReceiverRunner implements Runnable {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			notifyListenSocket = IPResolver.configureHostListeningSocket();
			while(true){
				try{
					Socket clientSocket = notifyListenSocket.accept();
					if(clientSocket != null){
						DataInputStream in= new DataInputStream(clientSocket.getInputStream());
						DataOutputStream out =new DataOutputStream(clientSocket.getOutputStream());
						
						
						String r_message = in.readUTF();
						
						if(r_message.startsWith("CONSENTINVOKEEHEADER-")){
							String [] res = r_message.split("CONSENTINVOKEEBODY-");
							//String part0 = res[0];
							String body = res[1];
							System.out.println(body);
							Display.invokeeOut = out;
							Display.invokeeIn = in;
							Display.receivingConsentInput = true;
							//	if(Display.scanner.)
								//Display.scanner.close();
								
								//Scanner nScanner = new Scanner(System.in);
								//String reply = nScanner.nextLine();
								
							
							//nScanner.close();
							//closeConnection(clientSocket);
							
							//Display.scanner = new Scanner(System.in);
							
						}else if(r_message.startsWith("CONSENTHEADER-")){
							String [] res = r_message.split("CONSENTBODY-");
							//String part0 = res[0];
							String body = res[1];
							System.out.println(body);
							//Initialiser.receivingInput = true;
						//	if(Display.scanner.)
							String reply = Display.scanner.next();
							while(!reply.equalsIgnoreCase("yes") && !reply.equalsIgnoreCase("no")
									&& !reply.equalsIgnoreCase("y") && !reply.equalsIgnoreCase("n")){
								System.out.println("Please answer yes or no");
								reply = Display.scanner.next();
							}
							
							out.writeUTF(reply);
							
							String ack = in.readUTF();
							System.out.println(ack);
							
						}
						
						
						else if(r_message.startsWith("NOTICEHEADER-")){
							String [] res = r_message.split("NOTICEBODY-");
							//String part0 = res[0];
							String body = res[1];
							System.out.println(body);
							
							
						}
						else if (r_message.contains("ack")){
							System.out.println(r_message);
						}
					}
					
					
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
		
	}

	private boolean hasAccess(String accessLevel, String ip, String host_ip){
		
		if(accessLevel.equalsIgnoreCase("ALL"))
			return true;
		else if(accessLevel.equalsIgnoreCase("SUBNET")){
			
			if(ip.substring(0,ip.lastIndexOf(".")).equalsIgnoreCase(host_ip.substring(0,ip.lastIndexOf(".")))){
				return true;
			}else
				return false;
		}else if(accessLevel.equalsIgnoreCase("IP")){
			if(ip.equalsIgnoreCase(host_ip)){
				return true;
			}else return false;
		}
		
		return false;
	}
	
	private void closeConnection(Socket clientSocket){
		if (clientSocket != null){
			try {
				clientSocket.close();
				clientSocket = null;
			} catch (IOException e) {
				/* close failed */
			}
		}
	}
}