package org.gla.weather.server;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.ArrayList;

import mcom.bundle.AccessLevel;
import mcom.bundle.ContractType;
import mcom.bundle.SensitivityLevels;
import mcom.bundle.SessionType;
import mcom.bundle.annotations.mController;
import mcom.bundle.annotations.mControllerInit;
import mcom.bundle.annotations.mEntity;
import mcom.bundle.annotations.mEntityContract;
import mcom.entity.MComEntity;
import mcom.exceptions.ContractNotFoundException;
import mcom.bundle.Policy;
import mcom.util.McomUtils;
import net.aksingh.java.api.owm.CurrentWeatherData;
import net.aksingh.java.api.owm.OpenWeatherMap;

import org.json.JSONException;
import org.w3c.dom.Document;

//@mSession(type=SessionType.STATEFUL)
@mController(bundleName = "wbundle.jar")
@mEntity(sessionType = SessionType.PERSISTENT)
public class WeatherBundle  extends MComEntity implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */

	public static void main(String[] args) {

		String w = new WeatherBundle().getWeather("Glasgow");
		System.out.println(w);
	}

	private String lastCity;
	static final String desc = "Enter the name of a city (e.g. Glasgow)";

	@mEntityContract(description = desc, contractType = ContractType.GET, requiredContracts = "storeToList",accessLevel = AccessLevel.ALL, sensitivityLevel = SensitivityLevels.PRIVATE)
	@mControllerInit
	public String getWeather(String city) {
		String result = "";
		System.out.println("Requested city: " + city);
		city = city.substring(0, city.indexOf("|"));
		city = city.trim();
		// declaring object of "OpenWeatherMap" class
		OpenWeatherMap owm = new OpenWeatherMap("");
		Policy policy;
		if(this.originPolicy != null)
			policy = this.originPolicy;
		else
			policy = McomUtils.getPolicy(this.bundleName);
		try {
			CurrentWeatherData cwd = owm.currentWeatherByCityName(city);
			// printing city name from the retrieved data
			result = "| " + result + "City: " + cwd.getCityName() + " | ";

			// printing the max./min. temperature
			double c_temp = (cwd.getMainData_Object().getTemperature() - 32) / 1.8000;
			double c_temp_ = (double) Math.round(c_temp * 100) / 100;
			result = result + "Temperature: " + c_temp_ + "\'C | ";

			if (cwd.getRain_Object().hasRain3Hours()) {
				result = result + "Rain in 3hrs: "
						+ cwd.getRain_Object().getRain3Hours() + " | ";
			} else {
				result = result + "Rain in 3hrs: - | ";
			}
			if (cwd.getWind_Object().hasWindSpeed()) {
				result = result + "Current wind speed: "
						+ cwd.getWind_Object().getWindSpeed() + " | ";
			} else {
				result = result + "Current wind speed: - | ";

			}
			if (cwd.getClouds_Object().hasPercentageOfClouds()) {
				result = result + "Cloudy: "
						+ cwd.getClouds_Object().getPercentageOfClouds()
						+ "% | ";
			} else {
				result = result + "Cloudy: -% | ";
			}
			/*ArrayList<String> availableBundles = McomUtils.getAvailableBundlesforContract("storeToList");
			
			if(availableBundles == null || availableBundles.isEmpty()){
				try {
					throw new ContractNotFoundException("Contract getWeather not found");
				} catch (ContractNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			Document doc = McomUtils.decodeTextToXml(availableBundles.get(0));
			boolean consent = false;
			if(policy.isConsent()){
				consent = McomUtils.seekConsent(this, "getWeather", "storeToList", doc);
			}else
				consent = true;
			if(consent){
				McomUtils.invokeContract(this, "getWeather", bundleName, availableBundles.get(0), "storeToList", new String[]{city} );
			}
			if(policy.isNotice()){
				McomUtils.sendNotice(this, "getWeather", "storeToList", doc);
			}*/

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		lastCity = city;
		return result;
	}

	@Override
	public String toString() {
		return "Weather Object, Last city is " + lastCity;
	}

	public String getLastCity() {
		return lastCity;
	}

	public void setLastCity(String lastCity) {
		this.lastCity = lastCity;
	}

	public static String getDesc() {
		return desc;
	}

	@mEntityContract(description = "Testing Access Control", contractType = ContractType.GET, accessLevel = AccessLevel.IP, sensitivityLevel = SensitivityLevels.PRIVATE)
	public String getSecret() {
		return "I'm scared";
	}
}
